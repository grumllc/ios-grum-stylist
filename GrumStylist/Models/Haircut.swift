//
//  Haircut.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/14/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

struct Haircut {
    var haircutID:String?
    var stylistID:String?
    var stylist:String?
    var clientID:String?
    var shortDescription:String?
    var dateTaken:String?
    var snapshots:[Snapshot]? // array of the 4 snapshots
    
    init() {
        self.haircutID = UUID().uuidString
        self.snapshots = [Snapshot]()
    }
    
    init(withDict dict:[String:Any]) {
        self.haircutID = dict["haircutID"] as? String
        self.stylistID = dict["stylistID"] as? String
        self.stylist = dict["stylist"] as? String
        self.clientID = dict["clientID"] as? String
        self.shortDescription = dict["shortDescription"] as? String
        self.dateTaken = dict["dateTaken"] as? String
        
        guard let snapshotsDict = dict["snapshots"] as? [String:Any] else { return }
        var snapshots = [Snapshot]()
        for (_, value) in snapshotsDict {
            let snapshot = Snapshot(withDict: value as! [String:Any])
            snapshots.append(snapshot)
        }
        self.snapshots = snapshots
    }
}
