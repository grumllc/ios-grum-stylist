//
//  User.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/11/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import Foundation

public class User {
    // MARK: - Properties
    var displayName:String?
    var uid:String?
    var email:String?
    var totalHaircuts:Int
    
    var dictionary: [String: Any] {
        return [
            "displayName": displayName!,
            "uid": uid!,
            "email": email!,
            "totalHaircuts": totalHaircuts,
        ]
    }
    
    // MARK: - Initialization
    init(withDict dict:[String:Any]) {
        self.displayName = dict["displayName"] as? String
        self.uid = dict["uid"] as? String
        self.email = dict["email"] as? String
        self.totalHaircuts = dict["totalHaircuts"] as! Int
    }
}
