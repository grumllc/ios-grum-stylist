//
//  FormInput.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/18/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

enum FormInputeType {
    case textField
    case textView
    case datePicker
}
struct FormInput {
    var title:String?
    var type:FormInputeType?
    var text:String?
    let setValue: ((String) -> Void)?
}
