//
//  Snapshot.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/21/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

enum SnapshotPositione {
    case unknown
    case front
    case back
    case left
    case right
    
    var toString : String {
        switch self {
        case .front: return "front"
        case .back: return "back"
        case .left: return "left"
        case .right: return "right"
        case .unknown: return "unknown"
        }
    }
}

class SnapshotPosition {
    static func stringToEnum(enumString:String) -> SnapshotPositione {
        switch enumString {
        case "front":
            return .front
        case "back":
            return .back
        case "left":
            return .left
        case "right":
            return .right
        default:
            return .unknown
        }
    }
}

class Snapshot: NSObject {
    // MARK: - Properties
    public var image:UIImage?
    public var notes:String?
    public var haircutID:String?
    public var position:SnapshotPositione?
    public var imageURL:String?
    
    var dictionary: [String: Any] {
        return [
            "notes": self.notes!,
            "haircutID": self.haircutID!,
            "position": self.position!.toString,
            "imageURL": self.imageURL!,
           ]
    }
    
    // MARK: - Initialization
    override init() {
        super.init()
    }
    
    init(image:UIImage, notes:String, position:SnapshotPositione) {
        self.notes = notes
        self.image = image
        self.position = position
    }
    
    init(withDict dict:[String:Any]) {
        self.notes = dict["notes"] as? String
        self.haircutID = dict["haircutID"] as? String
        self.imageURL = dict["imageURL"] as? String
        self.position = SnapshotPosition.stringToEnum(enumString: dict["position"] as! String)
    }

    // MARK: - Private API
    
    // MARK: - Public API
    
}
