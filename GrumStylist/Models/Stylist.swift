//
//  Company.swift
//  GrumStylist
//
//  Created by Daniel Yo on 1/19/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import FirebaseAuth

class Stylist: NSObject {
    // MARK: - Properties
    static let currentStylist = Stylist()
    
    public var displayName:String?
    public var stylistID:String? // unique, used to log in
    public var contactEmail:String?
    public var password:String?

    var dictionary: [String: Any] {
        return [
            "displayName": displayName!,
            "stylistID": stylistID!,
            "contactEmail": contactEmail!,
            "password": password!,
        ]
    }
    
    // MARK: - Initialization
    private func customInit() {
        
    }
    override init() {
        super.init()
        self.customInit()
    }
    
    convenience init(withDict dict:[String:Any]) {
        self.init()
        self.displayName = dict["displayName"] as? String
        self.stylistID = dict["stylistID"] as? String
        self.contactEmail = dict["contactEmail"] as? String
        self.password = dict["password"] as? String
    }
    // MARK: - Private API
    
    // MARK: - Public API
    func logout() {
        do {
            try Auth.auth().signOut()
            self.displayName = nil
            self.stylistID = nil
            self.contactEmail = nil
            self.password = nil
            UserDefaults.standard.set(nil, forKey: UserDefaultKeys.stylistID)
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    // MARK: - Delegates
}
