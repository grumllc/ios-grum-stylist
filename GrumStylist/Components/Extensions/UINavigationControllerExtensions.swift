//
//  UINavigationControllerExtensions.swift
//  Grum
//
//  Created by Daniel Yo on 1/9/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
