//
//  UIViewExtensions.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/11/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

extension UIView {
    
    func isSurroundingItem(itemBounds rect: CGRect) -> Bool {
        let endX = rect.origin.x + rect.size.width
        let endY = rect.origin.y + rect.size.height
        let viewRightEdgeX = self.frame.origin.x + self.frame.size.width
        let viewRightEdgeY = self.frame.origin.y + self.frame.size.height
        
        //print(self.frame)
        if (rect.origin.x >= self.frame.origin.x && rect.origin.y >= self.frame.origin.y &&
            endX <= viewRightEdgeX && endY <= viewRightEdgeY) {
            return true
        }
        return false
    }
}
