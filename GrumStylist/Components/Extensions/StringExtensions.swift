//
//  StringExtensions.swift
//  Grum
//
//  Created by Daniel Yo on 1/11/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//


extension Optional where Wrapped == String {
    
    public var isNullOrEmpty:Bool {
        if (self == nil || self == "") { return true }
        return false
    }
}
