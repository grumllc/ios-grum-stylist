//
//  UserDefaultKeys.swift
//  Grum
//
//  Created by Daniel Yo on 1/9/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class UserDefaultKeys {
    static let isLoggedIn:String = "isLoggedIn"
    static let stylistID:String = "stylistID"


}
