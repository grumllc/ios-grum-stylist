//
//  AppTheme.swift
//  Grum
//
//  Created by Daniel Yo on 1/9/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class AppTheme {
    public func mainColor() -> UIColor {
        return UIColor(hexString: "#FBAC03")
    }
    
    public func secondaryColor() -> UIColor {
        return UIColor(hexString: "#0097AA")
    }
}
