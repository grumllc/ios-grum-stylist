//
//  NotificationKeys.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/25/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import Foundation

class NotificationKeys {
    static let didFinishTakingSnapshots = Notification.Name("didFinishTakingSnapshots")
    static let didSaveHaircut = Notification.Name("didSaveHaircut")
}
