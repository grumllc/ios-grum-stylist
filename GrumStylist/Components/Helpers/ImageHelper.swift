//
//  ImageHelper.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/28/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class ImageHelper: NSObject {
    static let imageCache = NSCache<AnyObject, AnyObject>()

    static func getImage(withURLString urlString:String, success:@escaping (UIImage?) -> (), failure:@escaping (String) -> ())  {
        let url = URL(string: urlString)
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            success(cachedImage)
        } else {
            
            let urlSession = URLSession.shared
            urlSession.dataTask(with: url!) { (data, response, error) in
                if error != nil {
                    failure(error.debugDescription)
                    return
                }
                let image = UIImage(data: data!)
                self.imageCache.setObject(image!, forKey: urlString as AnyObject)
                success(image)
                
            }.resume()

        }
        
    }
}
