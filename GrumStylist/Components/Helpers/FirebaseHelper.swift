//
//  FirebaseHelper.swift
//  Grum
//
//  Created by Daniel Yo on 1/8/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import Firebase

class FirebaseHelper {

    // MARK: - Properties
    static let shared = FirebaseHelper()

    // MARK: - Initialization
    private init() {
    }
    
    // MARK: - Private API

    
    // MARK: - Public API
    func configure() {
        let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
        let options = FirebaseOptions(contentsOfFile: filePath)
        FirebaseApp.configure(options: options!)
    }
    // MARK: - Delegates
}
