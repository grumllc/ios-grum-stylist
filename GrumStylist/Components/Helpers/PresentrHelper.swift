//
//  PresentrHelper.swift
//  Grum
//
//  Created by Daniel Yo on 1/14/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import Presentr

class PresentrHelper {
    private let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.dismissTransitionType = .coverHorizontalFromRight
        return presenter
    }()
    
    // MARK: - Public API
    public func displaySimpleAlert(withTitle title:String, body:String, parentVC:UIViewController, handler:(() -> Void)?) {
        let alertController = AlertViewController(title: title, body: body)

        let okAction = AlertAction(title: "Ok", style: .destructive, handler: handler)
        alertController.addAction(okAction)
        parentVC.customPresentViewController(presenter, viewController: alertController, animated: true, completion: nil)

    }
    
    public func displayAlertWithCallback(callback:@escaping () -> (), callbackTitle:String, title:String, body:String, parentVC:UIViewController) {
        let alertController = AlertViewController(title: title, body: body)
        
        let cancelAction = AlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let callbackAction = AlertAction(title: callbackTitle, style: .default, handler: callback)
        alertController.addAction(callbackAction)
        
        parentVC.customPresentViewController(presenter, viewController: alertController, animated: true)
    }
}
