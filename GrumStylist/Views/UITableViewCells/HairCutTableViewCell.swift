//
//  HairCutTableViewCell.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/13/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class HairCutTableViewCell: UITableViewCell {

    // MARK: - Properties
    private var _haircut: Haircut = Haircut()
    public var haircut:Haircut {
        get { return _haircut }
        set {
            _haircut = newValue
            updateCell()
        }
    }
    private var imageViewPhoto: ImageWithIndicatorView = {
        let imageview = ImageWithIndicatorView()
        imageview.clipsToBounds = true
        return imageview
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    
    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.black.withAlphaComponent(0.4)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    private let stylistLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.black
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.textAlignment = .left
        return label
    }()
    
    override func prepareForReuse() {
        
    }
    
    // MARK: - Initialization
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted == true {
            self.contentView.backgroundColor = AppTheme().mainColor().withAlphaComponent(0.4)
        } else {
            self.contentView.backgroundColor = .clear
        }
    }
    
    // MARK: - Private API
    private func setup() {
        
        self.selectionStyle = .none
        
        self.addSubview(imageViewPhoto)
        imageViewPhoto.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.top.equalToSuperview().offset(10)
            make.width.equalTo(100)
            make.height.equalTo(80)
        }
        
        self.addSubview(dateLabel)
        dateLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalTo(imageViewPhoto.snp.right).offset(10)
            make.right.equalToSuperview()
        }
        
        self.addSubview(descriptionLabel)
        descriptionLabel.snp.makeConstraints { (make) in
            make.top.equalTo(dateLabel.snp.bottom)
            make.left.equalTo(imageViewPhoto.snp.right).offset(10)
            make.right.equalToSuperview().offset(-10)
        }
        
        self.addSubview(stylistLabel)
        stylistLabel.snp.makeConstraints { (make) in
            make.top.equalTo(descriptionLabel.snp.bottom)
            make.left.equalTo(imageViewPhoto.snp.right).offset(10)
            make.right.equalToSuperview()
        }
    }
    
    private func updateCell() {
        
        // date label
        dateLabel.text = haircut.dateTaken
        
        // description
        descriptionLabel.text = haircut.shortDescription
        
        // stylist name
        stylistLabel.text = haircut.stylist
        
        // front snapshot
        getFrontSnapshot()
    }
    
    private func getFrontSnapshot() {        
        guard let snapshots = haircut.snapshots else { return }
        for snapshot in snapshots {
            if snapshot.position == .front {
                if let urlString = snapshot.imageURL {
                    self.imageViewPhoto.getImage(withURLString: urlString)
                    return
                }
            }
        }
        
    }
    // MARK: - Public API
    
    // MARK: - Delegates


}
