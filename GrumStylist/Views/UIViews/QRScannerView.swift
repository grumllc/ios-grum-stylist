//
//  QRScannerView.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/3/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class QRScannerView: UIView {

    // MARK: - Properties
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
    }
    
    // MARK: - Public API
    public func updateFrameShadow() {
        DispatchQueue.main.async {
            self.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            do {
                // top left
                let path = UIBezierPath()
                path.move(to: .zero)
                path.addLine(to: CGPoint(x: 0, y: 40))
                path.addLine(to: .zero)
                path.addLine(to: CGPoint(x: 40, y: 0))
                
                let shape = CAShapeLayer()
                shape.path = path.cgPath
                shape.strokeColor = UIColor.white.cgColor
                shape.lineWidth = 2
                
                shape.shadowColor = UIColor.blue.cgColor
                shape.shadowRadius = 5
                shape.shadowOpacity = 1
                shape.shadowOffset = CGSize(width: 0.0, height: 0.0)
                
                self.layer.addSublayer(shape)
            }
            
            // Top Right
            do {
                let right = self.bounds.size.width
                
                let path = UIBezierPath()
                path.move(to: CGPoint(x: right, y: 0))
                path.addLine(to: CGPoint(x: right, y: 40))
                path.addLine(to: CGPoint(x: right, y: 0))
                path.addLine(to: CGPoint(x: right - 40, y: 0))
                
                let shape = CAShapeLayer()
                shape.path = path.cgPath
                shape.strokeColor = UIColor.white.cgColor
                shape.lineWidth = 2
                
                shape.shadowColor = UIColor.blue.cgColor
                shape.shadowRadius = 5
                shape.shadowOpacity = 1
                shape.shadowOffset = CGSize(width: 0.0, height: 0.0)
                
                self.layer.addSublayer(shape)
            }
            
            // Bottom Left
            do {
                let bottom = self.bounds.size.height
                
                let path = UIBezierPath()
                path.move(to: CGPoint(x: 0, y: bottom))
                path.addLine(to: CGPoint(x: 0, y: bottom - 40))
                path.addLine(to: CGPoint(x: 0, y: bottom))
                path.addLine(to: CGPoint(x: 40, y: bottom))
                
                let shape = CAShapeLayer()
                shape.path = path.cgPath
                shape.strokeColor = UIColor.white.cgColor
                shape.lineWidth = 2
                
                shape.shadowColor = UIColor.blue.cgColor
                shape.shadowRadius = 5
                shape.shadowOpacity = 1
                shape.shadowOffset = CGSize(width: 0.0, height: 0.0)
                
                self.layer.addSublayer(shape)
            }
            
            // Bottom Right
            do {
                let bottom = self.bounds.size.height
                let right = self.bounds.size.width
                
                let path = UIBezierPath()
                path.move(to: CGPoint(x: right, y: bottom))
                path.addLine(to: CGPoint(x: right, y: bottom - 40))
                path.addLine(to: CGPoint(x: right, y: bottom))
                path.addLine(to: CGPoint(x: right - 40, y: bottom))
                
                let shape = CAShapeLayer()
                shape.path = path.cgPath
                shape.strokeColor = UIColor.white.cgColor
                shape.lineWidth = 2
                
                shape.shadowColor = UIColor.blue.cgColor
                shape.shadowRadius = 5
                shape.shadowOpacity = 1
                shape.shadowOffset = CGSize(width: 0.0, height: 0.0)
                
                self.layer.addSublayer(shape)
            }
        }
        
    }
    
    
    // MARK: - Delegates

}
