//
//  SnapshotPhotoView.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/26/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol SnapshotPhotoViewDelegate: class {
    func SnapshotPhotoViewDelegate(didTap sender:SnapshotPhotoView)
}
class SnapshotPhotoView: UIView {

    // MARK: - Properties
    public weak var delegate: SnapshotPhotoViewDelegate?
    private var _image:UIImage = UIImage()
    public var image: UIImage {
        get { return _image }
        set {
            _image = newValue
            self.imageview.image = newValue
        }
    }
    private var _position: SnapshotPositione = .front
    public var position: SnapshotPositione {
        get { return _position }
        set {
            _position = newValue
            switch newValue {
            case .front:
                self.label.text = "FRONT"; break;
            case .back:
                self.label.text = "BACK"; break;
            case .left:
                self.label.text = "LEFT"; break;
            case .right:
                self.label.text = "RIGHT"; break
            case .unknown:
                break
            }
        }
    }
    
    private let imageview: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        return imageview
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()
    
    let imageviewExpand: UIImageView = {
        let imageview = UIImageView()
        imageview.image = UIImage(named: "snapshot_expand")!.withRenderingMode(.alwaysTemplate)
        imageview.tintColor = .white
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    // MARK: - Initialization
    init(withImage image:UIImage) {
        super.init(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
        self.imageview.image = image
        setup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
        self.addSubview(imageview)
        imageview.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.addSubview(label)
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-10)
            make.centerX.equalToSuperview()
        }
        
        self.addSubview(imageviewExpand)
        imageviewExpand.snp.makeConstraints { (make) in
            make.bottom.right.equalToSuperview().offset(-5)
            make.width.height.equalTo(40)
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(view_didTap))
        self.addGestureRecognizer(tapGesture)
        
    }
    
    @objc private func view_didTap(gestureRecognizer: UIGestureRecognizer) {
        self.delegate?.SnapshotPhotoViewDelegate(didTap: self)
    }
    
    // MARK: - Public API
    
}
