//
//  ImageWithIndicatorView.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/28/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class ImageWithIndicatorView: UIView {

    // MARK: - Properties
    let imageCache = NSCache<AnyObject, AnyObject>()
    private let defaultImage = UIImage(named: "snapshot_missing")
    private var urlString: String?
    private let imageView: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        return imageview
    }()
    private let activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        indicator.startAnimating()
        return indicator
    }()
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
        // activity indicator
        self.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        self.addSubview(imageView)
        imageView.isHidden = true
        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    
    // MARK: - Public API
    public func getImage(withURLString urlString:String)  {
        self.urlString = urlString
        let url = URL(string: urlString)
        imageView.image = nil
        activityIndicator.isHidden = false
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            DispatchQueue.main.async {
                if self.urlString == urlString {
                    self.imageView.image = cachedImage
                    self.imageView.isHidden = false
                    self.activityIndicator.isHidden = true
                }
            }
        } else {
            
            let urlSession = URLSession.shared
            urlSession.dataTask(with: url!) { (data, response, error) in
                if error != nil {
                    return
                }

                guard let image = UIImage(data: data!) else { return }
                self.imageCache.setObject(image, forKey: urlString as AnyObject)
                DispatchQueue.main.async {
                    if self.urlString == urlString { // prevent view from loading a different image
                        self.imageView.image = image
                        self.imageView.isHidden = false
                        self.activityIndicator.isHidden = true
                    }
                }
                }.resume()
            
        }
        
    }
    // MARK: - Delegates

}
