//
//  CustomSegmentedControlView.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/13/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit


protocol CustomSegmentedControlViewDelegate: class {
    func CustomSegmentedControlViewDelegateDidSelectItem(name:String, index: Int)
}

class CustomSegmentedControlView: UIView {

    class SegmentView {
        var view: UIView?
        var label:UILabel?
        
        init(withView view:UIView, label:UILabel) {
            self.view = view
            self.label = label
        }
    }
    
    // MARK: - Properties
    public weak var delegate: CustomSegmentedControlViewDelegate?
    public var selectedItemIndex: Int = 0
    private let stackview: UIStackView = {
        let stackview = UIStackView()
        stackview.axis = .horizontal
        return stackview
    }()
    private var titles: [String] = []
    private var semgentViews = [SegmentView]()
    
    // MARK: - Initialization
    convenience init(withTitles titles:[String], delegate:CustomSegmentedControlViewDelegate) {
        self.init()
        self.delegate = delegate
        self.titles = titles
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let itemWidth = self.frame.width / CGFloat(titles.count)
        for (index, title) in titles.enumerated() {
            let view = UIView()
            let label = UILabel()
            label.text = title
            label.textAlignment = .center
            if index == 0 {
                label.textColor = AppTheme().mainColor()
            } else {
                label.textColor = UIColor.black.withAlphaComponent(0.4)
            }
            label.font = UIFont.boldSystemFont(ofSize: 15)
            view.addSubview(label)
            
            label.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
            }
            
            self.stackview.addArrangedSubview(view)
            view.snp.makeConstraints { (make) in
                make.width.equalTo(itemWidth)
                make.top.bottom.equalToSuperview()
            }
            
            // add gesture tap
            view.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(segmentView_touchUpInside))
            view.addGestureRecognizer(tap)
            
            self.semgentViews.append(SegmentView(withView: view, label: label))
        }
    }
    
    // MARK: - Private API
    private func setup() {
        self.addSubview(self.stackview)
        self.stackview.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    // MARK: UIResponder
    @objc private func segmentView_touchUpInside(sender: UIGestureRecognizer) {
        let view = sender.view
        for (index, segmentView) in self.semgentViews.enumerated() {
            
            if segmentView.view == view {
                // no need to continue if we're already on the selected index
                if selectedItemIndex == index { return }
                
                guard let text = segmentView.label?.text else { return }
                selectedItemIndex = index
                self.delegate?.CustomSegmentedControlViewDelegateDidSelectItem(name: text, index: index)
                segmentView.label?.textColor = AppTheme().mainColor()
                
            } else {
                segmentView.label?.textColor = UIColor.black.withAlphaComponent(0.4)
            }
        }
    }
    
    // MARK: - Public API

    
    // MARK: - Delegates

}
