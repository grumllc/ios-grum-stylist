//
//  RoundedImageView.swift
//  Grum
//
//  Created by Daniel Yo on 2/11/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class RoundedImageView: UIView {
    
    // MARK: - Properties
    public let imageView:UIImageView = {
        let image = UIImage.init(named: "profile_empty")!.withRenderingMode(.alwaysTemplate)
        
        let imageview = UIImageView.init(image: image)
        imageview.tintColor = UIColor.black.withAlphaComponent(0.15)
        imageview.contentMode = .scaleAspectFit
        imageview.clipsToBounds = true
        return imageview
    }()
    
    // MARK: - Initialization
    init(width:CGFloat, height:CGFloat) {
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: height))
        self.setup()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        self.backgroundColor = .white
        self.layer.masksToBounds = true
        self.layer.cornerRadius = self.frame.width / 2
        
        // imageview
        self.addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
    }
    // MARK: - Public API
    
    // MARK: - Delegates
    
}
