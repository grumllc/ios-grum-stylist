//
//  SnapshotDefaultView.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/25/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol SnapshotDefaultViewDelegate: class {
    func SnapshotDefaultViewDelegate(didTap sender: SnapshotDefaultView, position: SnapshotPositione)
}

class SnapshotDefaultView: UIView {

    // MARK: - Properties
    public weak var delegate: SnapshotDefaultViewDelegate?
    var position: SnapshotPositione?
    let button: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.setBackgroundColor(color: UIColor.black.withAlphaComponent(0.4), forState: UIControl.State.highlighted)
        button.addTarget(self, action: #selector(button_touchUpInside), for: .touchUpInside)
        return button
    }()
    let imageviewSnapshot: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    let imageviewPhoto: UIImageView = {
        let imageview = UIImageView()
        imageview.image = UIImage(named: "camera_add_photo")!.withRenderingMode(.alwaysTemplate)
        imageview.tintColor = AppTheme().secondaryColor()
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
        self.addSubview(button)
        button.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.addSubview(imageviewSnapshot)
        imageviewSnapshot.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.addSubview(imageviewPhoto)
        imageviewPhoto.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(5)
            make.width.height.equalTo(48)
        }
    }
    
    @objc private func button_touchUpInside(sender: UIButton) {
        guard let position = self.position else { return }
        self.delegate?.SnapshotDefaultViewDelegate(didTap: self, position:position)
    }
    // MARK: - Public API
    

}
