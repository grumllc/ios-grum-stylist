//
//  CustomActivityIndicatorView.swift
//  Grum
//
//  Created by Daniel Yo on 1/15/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SnapKit

class CustomActivityIndicatorView: UIView {

    // MARK: - Properties
    let activityIndicatorView = NVActivityIndicatorView(frame: CGRect())

    // MARK: - Initialization
    private func customInit() {
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customInit()
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.75)
        self.isHidden = true
        activityIndicatorView.color = UIColor.white
        activityIndicatorView.type = .ballTrianglePath
        self.addSubview(activityIndicatorView)
        self.activityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(60)
            make.height.equalTo(60)
        }
    }
    // MARK: - Public API
    public func animate() {
        if (!self.activityIndicatorView.isAnimating) {
            self.isHidden = false
            self.activityIndicatorView.startAnimating()
        }
    }
    
    public func stopAnimate() {
        if (self.activityIndicatorView.isAnimating) {
            self.isHidden = true
            self.activityIndicatorView.stopAnimating()
        }

    }
    // MARK: - Delegates
}
