//
//  TextViewCollectionViewCell.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/20/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol TextViewCollectionViewCellDelegate: class {
    func TextViewCollectionViewCellDelegate(isEditingTextView textView:UITextView)
    
}
class TextViewCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Properties
    public weak var delegate: TextViewCollectionViewCellDelegate?
    private var _input: FormInput?
    public var input: FormInput {
        get {
            if (_input == nil) {
                _input = FormInput(title: nil, type: nil, text: nil, setValue: nil)
            }
            return _input!
        }
        set {
            _input = newValue
            self.setup()
            self.updateData()
        }
    }
    
    private let textViewWrapper: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.black.cgColor
        view.layer.borderWidth = 1
        view.backgroundColor = .clear
        view.layer.cornerRadius = 4
        return view
    }()
    
    public let textView: UITextView = {
        let textview = UITextView()
        textview.backgroundColor = .white
        textview.font = UIFont.systemFont(ofSize: 17)
        return textview
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.3)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
        // label
        self.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(10)
        }
        
        // wrapper
        self.addSubview(self.textViewWrapper)
        self.textViewWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(self.label.snp.bottom).offset(5)
            make.left.equalToSuperview().offset(10)
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(-10)
            make.height.equalTo(90)
        }
        
        // toolbar for text view
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "clear", style: .plain, target: self, action: #selector(clearButton_Pressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButton_Pressed))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        self.textView.inputAccessoryView = toolbar
        
        // textview
        self.textView.delegate = self
        self.textViewWrapper.addSubview(self.textView)
        self.textView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview().offset(5)
            make.right.bottom.equalToSuperview().offset(-5)
        }
        
    }
    
    private func updateData() {
        self.label.text = self.input.title

    }
    
    // MARK: UIResponder
    @objc func doneButton_Pressed() {
        self.textView.endEditing(true)
    }
    
    @objc func clearButton_Pressed() {
        self.textView.text = ""
        self.textView.endEditing(true)
    }
    
    // MARK: - Public API
    
    // MARK: - Delegates
}

extension TextViewCollectionViewCell: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.delegate?.TextViewCollectionViewCellDelegate(isEditingTextView: textView)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.input.setValue!(textView.text)
    }
    
    // limit characters
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 80
    }
}
