//
//  SnapshotsCollectionViewCell.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/21/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol SnapshotsCollectionViewCellDelegate: class {
    func SnapshotsCollectionViewCellDelegate(didSelectItem position:SnapshotPositione, status:PhotoCaptureVieweStatus)
}
class SnapshotsCollectionViewCell: UICollectionViewCell {
    // MARK: - Properties
    public weak var delegate: SnapshotsCollectionViewCellDelegate?
    private var _snapshots:[Snapshot]?
    public var snapshots: [Snapshot] {
        get {
            if _snapshots == nil {
                _snapshots = [Snapshot]()
            }
            return _snapshots!
        }
        set {
            _snapshots = newValue
            self.updateImages()
        }
    }
    let kOffsetiPhone = 10
    
    private let label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.3)
        label.textAlignment = .center
        label.text = "Take Headshots"
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    let frontImageWrapper: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        return view
    }()
    let backImageWrapper: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        return view
    }()
    let leftImageWrapper: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        return view
    }()
    let rightImageWrapper: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.clipsToBounds = true
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        return view
    }()

    let frontDefaultView: SnapshotDefaultView = {
        let view = SnapshotDefaultView()
        view.imageviewSnapshot.image = UIImage(named: "snapshot_front")
        view.position = .front
        return view
    }()
    
    let backDefaultView: SnapshotDefaultView = {
        let view = SnapshotDefaultView()
        view.position = .back
        view.imageviewSnapshot.image = UIImage(named: "snapshot_front")
        return view
    }()
    
    let leftDefaultView: SnapshotDefaultView = {
        let view = SnapshotDefaultView()
        view.position = .left
        view.imageviewSnapshot.image = UIImage(named: "snapshot_left")
        return view
    }()
    
    let rightDefaultView: SnapshotDefaultView = {
        let view = SnapshotDefaultView()
        view.position = .right
        view.imageviewSnapshot.image = UIImage(named: "snapshot_right")
        return view
    }()
    
    let frontPhotoView: SnapshotPhotoView = {
        let view = SnapshotPhotoView()
        view.position = .front
        view.isHidden = true
        return view
    }()
    let backPhotoView: SnapshotPhotoView = {
        let view = SnapshotPhotoView()
        view.position = .back
        view.isHidden = true
        return view
    }()
    let leftPhotoView: SnapshotPhotoView = {
        let view = SnapshotPhotoView()
        view.position = .left
        view.isHidden = true
        return view
    }()
    let rightPhotoView: SnapshotPhotoView = {
        let view = SnapshotPhotoView()
        view.position = .right
        view.isHidden = true
        return view
    }()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
        if AppUtility.isDeviceiPad() {
            self.iPadSetup()
        } else {
            self.iPhoneSetup()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func iPadSetup() {
        // TODO: iPad Setup differently for future feature
        self.iPhoneSetup()

        
    }
    private func iPhoneSetup() {
        let offsetWidth = CGFloat(kOffsetiPhone * 2 - 5)
        let width = (self.frame.width / 2) - offsetWidth
        
        // label
        self.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(kOffsetiPhone)
        }
        
        // front
        self.addSubview(frontImageWrapper)
        frontImageWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(label.snp.bottom).offset(kOffsetiPhone / 2)
            make.left.equalToSuperview().offset(kOffsetiPhone)
            make.width.height.equalTo(width)
        }
        // front default view
        self.frontImageWrapper.addSubview(self.frontDefaultView)
        self.frontDefaultView.delegate = self
        self.frontDefaultView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        // front photo view
        self.frontImageWrapper.addSubview(self.frontPhotoView)
        self.frontPhotoView.delegate = self
        self.frontPhotoView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        
        // back
        self.addSubview(backImageWrapper)
        backImageWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(label.snp.bottom).offset(kOffsetiPhone / 2)
            make.left.equalTo(frontImageWrapper.snp.right).offset(kOffsetiPhone / 2)
            make.right.equalToSuperview().offset(-kOffsetiPhone)
            make.height.equalTo(width)
        }
        // back default view
        self.backImageWrapper.addSubview(self.backDefaultView)
        self.backDefaultView.delegate = self
        self.backDefaultView.snp.makeConstraints { (make) in
            make.bottom.top.equalToSuperview()
            make.right.equalToSuperview()
            make.width.equalTo(frontDefaultView.snp.width)
        }
        // back photo view
        self.backImageWrapper.addSubview(self.backPhotoView)
        self.backPhotoView.delegate = self
        self.backPhotoView.snp.makeConstraints { (make) in
            make.bottom.top.equalToSuperview()
            make.right.equalToSuperview()
            make.width.equalTo(frontDefaultView.snp.width)
        }
        
        // left
        self.addSubview(leftImageWrapper)
        leftImageWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(frontImageWrapper.snp.bottom).offset(kOffsetiPhone / 2)
            make.left.equalToSuperview().offset(kOffsetiPhone)
            make.width.height.equalTo(width)
            make.bottom.equalToSuperview().offset(-5)
        }
        // left default view
        self.leftImageWrapper.addSubview(self.leftDefaultView)
        self.leftDefaultView.delegate = self
        self.leftDefaultView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        // left photo view
        self.leftImageWrapper.addSubview(self.leftPhotoView)
        self.leftPhotoView.delegate = self
        self.leftPhotoView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // right
        self.addSubview(rightImageWrapper)
        rightImageWrapper.snp.makeConstraints { (make) in
            make.top.equalTo(backImageWrapper.snp.bottom).offset(kOffsetiPhone / 2)
            make.left.equalTo(leftImageWrapper.snp.right).offset(kOffsetiPhone / 2)
            make.right.equalToSuperview().offset(-kOffsetiPhone)
            make.height.equalTo(width)
            make.bottom.equalToSuperview().offset(-5)
        }
        // right default view
        self.rightImageWrapper.addSubview(self.rightDefaultView)
        self.rightDefaultView.delegate = self
        self.rightDefaultView.snp.makeConstraints { (make) in
            make.bottom.top.equalToSuperview()
            make.right.equalToSuperview()
            make.width.equalTo(frontDefaultView.snp.width)
        }
        // right photo view
        self.rightImageWrapper.addSubview(self.rightPhotoView)
        self.rightPhotoView.delegate = self
        self.rightPhotoView.snp.makeConstraints { (make) in
            make.bottom.top.equalToSuperview()
            make.right.equalToSuperview()
            make.width.equalTo(frontDefaultView.snp.width)
        }
        
    }
    
    private func updateImages() {
        for snapshot in self.snapshots {
            if let image = snapshot.image {
                switch snapshot.position! {
                case .front:
                    self.frontPhotoView.image = image
                    self.frontPhotoView.isHidden = false
                    self.frontDefaultView.isHidden = true
                    break
                case .back:
                    self.backPhotoView.image = image
                    self.backPhotoView.isHidden = false
                    self.backDefaultView.isHidden = true
                    break
                case .left:
                    self.leftPhotoView.image = image
                    self.leftPhotoView.isHidden = false
                    self.leftDefaultView.isHidden = true
                    break
                case .right:
                    self.rightPhotoView.image = image
                    self.rightPhotoView.isHidden = false
                    self.rightDefaultView.isHidden = true
                    break
                case .unknown:
                    break
                }
            }
        }
    }
    
    // MARK: UIResponder
    
    // MARK: - Public API
    
    // MARK: - Delegates
}

extension SnapshotsCollectionViewCell: SnapshotDefaultViewDelegate {
    func SnapshotDefaultViewDelegate(didTap sender: SnapshotDefaultView, position: SnapshotPositione) {
        // adding new
        self.delegate?.SnapshotsCollectionViewCellDelegate(didSelectItem: position, status:.adding)
    }
}

extension SnapshotsCollectionViewCell: SnapshotPhotoViewDelegate {
    func SnapshotPhotoViewDelegate(didTap sender: SnapshotPhotoView) {
        // viewing / editing
        self.delegate?.SnapshotsCollectionViewCellDelegate(didSelectItem: sender.position, status:.editing)
    }
}
