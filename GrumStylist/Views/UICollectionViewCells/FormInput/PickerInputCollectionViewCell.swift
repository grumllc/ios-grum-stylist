//
//  PickerInputCollectionViewCell.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/19/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol PickerInputCollectionViewCellDelegate: class {
    func PickerInputCollectionViewCellDelegate(isEditingTextField textfield:UITextField)
    func PickerInputCollectionViewCellDelegate(didPressDone textfield:UITextField)

}

class PickerInputCollectionViewCell: UICollectionViewCell {
    // MARK: - Properties
    private var _input: FormInput?
    public var input: FormInput {
        get {
            if (_input == nil) {
                _input = FormInput(title: nil, type: nil, text: nil, setValue: nil)
            }
            return _input!
        }
        set {
            _input = newValue
            self.setup()
            self.updateData()
        }
    }
    
    public weak var delegate: PickerInputCollectionViewCellDelegate?
    public let textField: UITextField = {
        let textfield = UITextField()
        textfield.tintColor = .clear
        textfield.textAlignment = .right
        textfield.backgroundColor = UIColor.clear
        return textfield
    }()
    
    private let datePicker: UIDatePicker = {
        let datepicker = UIDatePicker()
        datepicker.datePickerMode = .date
        return datepicker
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.3)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
        // label
        self.contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.height.equalTo(self.contentView.frame.height - 5)
        }
        
        // setup the textfield with picker
        self.textField.inputView = self.datePicker
        self.textField.delegate = self
        
        self.contentView.addSubview(self.textField)
        self.textField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.width.equalTo(self.contentView.frame.width - 20)
            make.centerY.equalToSuperview()
            make.height.equalTo(self.contentView.frame.height - 5)
        }
        
        // toolbar for picker view
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButton_Pressed))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButton_Pressed))

        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        textField.inputAccessoryView = toolbar
        
        // separator
        let separator = UIView()
        separator.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        self.contentView.addSubview(separator)
        separator.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
    }
    
    private func updateData() {
        if self.label.text == "" || self.label.text == nil {
            self.label.text = self.input.title
        }
        if self.textField.placeholder == "" || self.textField.placeholder == nil {
            self.textField.placeholder = self.input.title
        }
        if self.textField.text == "" || self.textField.text == nil {
            self.textField.text = self.input.text
        }
    }
    
    // MARK: UIResponder
    @objc func doneButton_Pressed() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        let value = formatter.string(from: datePicker.date)
        textField.text = value
        
        self.textField.endEditing(true)
        self.delegate?.PickerInputCollectionViewCellDelegate(didPressDone: self.textField)
    }
    
    @objc func cancelButton_Pressed() {
        self.textField.endEditing(true)
    }
    
    // MARK: - Public API
    
    // MARK: - Delegates
}

extension PickerInputCollectionViewCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.PickerInputCollectionViewCellDelegate(isEditingTextField: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.input.setValue!(text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
