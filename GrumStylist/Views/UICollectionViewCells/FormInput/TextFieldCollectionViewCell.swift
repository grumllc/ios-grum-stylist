//
//  TextFieldCollectionViewCell.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/18/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol TextFieldCollectionViewCellDelegate: class {
    func TextFieldCollectionViewCellDelegate(isEditingTextField textfield:UITextField)
    func TextFieldCollectionViewCellDelegate(didPressNext textfield:UITextField)
}

class TextFieldCollectionViewCell: UICollectionViewCell {
    // MARK: - Properties
    private var _input: FormInput?
    public var input: FormInput {
        get {
            if (_input == nil) {
                _input = FormInput(title: nil, type: nil, text: nil, setValue: nil)
            }
            return _input!
        }
        set {
            _input = newValue
            self.setup()
            self.updateData()
        }
    }
    public weak var delegate: TextFieldCollectionViewCellDelegate?
    public let textField: UITextField = {
        let textfield = UITextField()
        textfield.textAlignment = .right
        textfield.backgroundColor = UIColor.clear
        textfield.returnKeyType = .next
        textfield.clearButtonMode = .whileEditing
        return textfield
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black.withAlphaComponent(0.3)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 17)
        return label
    }()
    
    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private API
    private func setup() {
        
        // label
        self.contentView.addSubview(self.label)
        self.label.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.height.equalTo(self.contentView.frame.height - 5)
        }
        
        // txtfield
        self.contentView.addSubview(self.textField)
        self.textField.snp.makeConstraints { (make) in
            make.width.equalTo(300)
            make.right.equalToSuperview().offset(-10)
            make.centerY.equalToSuperview()
            make.height.equalTo(self.contentView.frame.height - 5)
            
        }
        self.textField.delegate = self
        
        // separator
        let separator = UIView()
        separator.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        self.contentView.addSubview(separator)
        separator.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(view_didTap))
        self.addGestureRecognizer(tapGesture)
    }
    
    private func updateData() {
        if self.label.text == "" || self.label.text == nil {
            self.label.text = self.input.title
        }
        if self.textField.placeholder == "" || self.textField.placeholder == nil {
            self.textField.placeholder = self.input.title
        }
        if self.textField.text == "" || self.textField.text == nil {
            self.textField.text = self.input.text
        }
    }
    
    // MARK: UIResponder
    @objc private func view_didTap(gestureRecognizer: UIGestureRecognizer) {
        self.textField.becomeFirstResponder()
    }
    
    // MARK: - Public API
    
}

extension TextFieldCollectionViewCell: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.TextFieldCollectionViewCellDelegate(isEditingTextField: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.input.setValue!(text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.delegate?.TextFieldCollectionViewCellDelegate(didPressNext: textField)
        return true
    }
}
