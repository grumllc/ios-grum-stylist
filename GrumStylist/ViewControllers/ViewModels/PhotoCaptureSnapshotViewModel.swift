//
//  PhotoCaptureSnapshotViewModel.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/26/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class PhotoCaptureSnapshotViewModel: NSObject {
    // MARK: - Properties
    private var _snapShotPosition: SnapshotPositione?
    public var snapShotPosition: SnapshotPositione {
        get {
            if (_snapShotPosition == nil) { _snapShotPosition = .front }
            return _snapShotPosition!
        }
        set {
            _snapShotPosition = newValue
            self.updateData()
            
        }
    }
    public var title: String?
    
    // MARK: - Initialization
    
    override init() {
        super.init()
    }
    
    // MARK: - Private API
    private func updateData() {
        
        var position: String = ""
        switch self.snapShotPosition {
        case .front:
            position = "FRONT"; break;
        case .back:
            position = "BACK"; break;
        case .left:
            position = "LEFT"; break;
        case .right:
            position = "RIGHT"; break
        case .unknown:
            break
        }
        self.title = "\(position)"
    }
    
    // MARK: - Public API

}
