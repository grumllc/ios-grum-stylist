//
//  AddHaircutViewModel.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/18/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class AddHaircutViewModel: NSObject {

    // MARK: - Properties
    public var inputs = [FormInput]()
    public var haircut: Haircut?
    private let dateFormatter: DateFormatter = {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM/dd/yyyy"
        return dateformatter
    }()
    // MARK: - Initialization
    override init() {
        super.init()
    }
    init(withClientID id:String) {
        super.init()

        self.haircut = Haircut()
        self.haircut?.clientID = id
        self.haircut?.stylistID = Stylist.currentStylist.stylistID
        self.haircut?.stylist = Stylist.currentStylist.displayName
        self.haircut?.dateTaken = self.dateFormatter.string(from: Date())
        self.setupData()
    }
    
    // MARK: - Private API
    private func setupData() {

        self.inputs.append(FormInput.init(title: "Stylist Name", type: .textField, text: self.haircut?.stylist, setValue: { [unowned self] (text) in
            self.haircut?.stylist = text
            self.inputs[0].text = text
        }))
        
        self.inputs.append(FormInput.init(title: "Date of Haircut", type: .datePicker, text: self.haircut?.dateTaken, setValue: { [unowned self] (text) in
            self.haircut?.dateTaken = text
        }))

        self.inputs.append(FormInput.init(title: "Short Description of Haircut (80 char)", type: .textView, text: nil, setValue: { [unowned self] (text) in
            self.haircut?.shortDescription = text
        }))
    }
    
   
    
    // MARK: - Public API
    public func areInputsValid() -> Bool {
        
        guard let name = self.haircut?.stylist else { return false }
        guard let shortDesc = self.haircut?.shortDescription else { return false }
        guard let dateString = self.haircut?.dateTaken else { return false }
        guard let snapshots = self.haircut?.snapshots else { return false }
        
        if name.trimmingCharacters(in: .whitespacesAndNewlines) == "" || shortDesc.trimmingCharacters(in: .whitespacesAndNewlines) == ""
            || dateString.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return false
        }
        
        if snapshots.count < 4 {
            return false
        }
        
        return true
    }
    
    public func save(withSuccess success:@escaping () -> (), failure: @escaping (String) -> ()) {
        
        var errorMessage:String? = nil
        let group = DispatchGroup()
        
        for snapshot in self.haircut!.snapshots! { // save the 4 images
            let snapshotImageRequestModel = SaveSnapshotImageRequestModel(withImage: snapshot.image!, position: snapshot.position!.toString, haircutID: self.haircut!.haircutID!)
            
            group.enter()
            FirebaseStorageManager().saveSnapshotImage(withRequestModel: snapshotImageRequestModel, success: { (urlString) in
                snapshot.imageURL = urlString
                group.leave()
            }) { (errorString) in
                errorMessage = errorString
                group.leave()
            }
        }
        
        // save the data after images are saved successfully
        group.notify(queue: .main) {
            if let message = errorMessage {
                failure(message + " Please try again later.")
            } else {
                let addHaircutRequestmodel = AddHaircutRequestModel(withHaircut: self.haircut!)
                FSHaircutManager().addHaircut(requestModel: addHaircutRequestmodel, success: {
                    NotificationCenter.default.post(name: NotificationKeys.didSaveHaircut, object: nil)
                    success()
                }) { (errorString) in
                    errorMessage = errorString
                }
                
            }
        }
    }
    
    // MARK: - Delegates
}
