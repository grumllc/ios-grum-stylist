//
//  PhotoCaptureSnapshotViewModel.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/22/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

enum PhotoCaptureVieweStatus {
    case editing
    case adding
}

class PhotoCaptureViewModel: NSObject {

    // MARK: - Properties
    public var photoCaptureViewStatus: PhotoCaptureVieweStatus?
    public var snapshots = [Snapshot]()
    private var _snapShotPosition: SnapshotPositione?
    public var snapShotPosition: SnapshotPositione {
        get {
            if (_snapShotPosition == nil) { _snapShotPosition = .unknown }
            return _snapShotPosition!
        }
        set {
            _snapShotPosition = newValue
            self.updateData()
            
        }
    }
    private var _title: String?
    public var title: String {
        get {
            if _title == nil {
                _title = ""
            }
            return _title!
        }
        set {
            _title = newValue
        }
    }
    
    // MARK: - Initialization

    override init() {
        super.init()
    }
    
    // MARK: - Private API
    private func updateData() {
        
        let resultForEditing = self.snapshots.filter { $0.position == snapShotPosition }
        self.photoCaptureViewStatus = resultForEditing.isEmpty ? .adding : .editing
        
        var position: String = ""
        switch self.snapShotPosition {
        case .front:
            position = "FRONT"; break;
        case .back:
            position = "BACK"; break;
        case .left:
            position = "LEFT"; break;
        case .right:
            position = "RIGHT"; break
        case .unknown:
            position = "UNKNOWN"; break
        }
        self.title = "\(position)"
    }
    
    // MARK: - Public API
    public func updateSnapshot(image: UIImage) {
        for snapshot: Snapshot in self.snapshots {
            if snapshot.position == self.snapShotPosition {
                snapshot.image = image
                return
            }
        }
        
        // we're adding new snapshot
        let snapshot = Snapshot(image: image, notes: "", position: self.snapShotPosition)
        self.snapshots.append(snapshot)
        
        if self.snapshots.count < 4 {
            // update to the next snapshot to take
            switch self.snapShotPosition {
            case .front:
                self.snapShotPosition = .back
                break
            case .back:
                self.snapShotPosition = .left
                break
            case .left:
                self.snapShotPosition = .right
                break
            case .right:
                self.snapShotPosition = .front
                break
            case .unknown:
                return
            }
        } else {
            // we've added all 4 snapshots
            NotificationCenter.default.post(name: NotificationKeys.didFinishTakingSnapshots, object: nil)
        }
       
    }
    
    // MARK: - Delegates
}
