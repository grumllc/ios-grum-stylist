//
//  CustomerDetailViewModel.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/13/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class CustomerDetailViewModel: NSObject {
    // MARK: - Properties
    public var haircuts = [Haircut]()
    public var user:User?
    public let itemsPerPage = 6
    private let haircutManager = FSHaircutManager()
    private let userManager = FSUserManager()
    private let storageManager = FirebaseStorageManager()
    
    // MARK: - Initialization
    private func customInit() {
        
    }
    override init() {
        super.init()
        self.customInit()
    }
    
    // MARK: - Private API
    
    // MARK: - Public API
    public func hasMoreRecords() -> Bool {
        if self.haircuts.count < self.user!.totalHaircuts {
            return true
        }
        return false
    }
    
    public func getUserImage(withUID uid:String, success:@escaping (UIImage)->(), failure:@escaping (String)->()) {
        storageManager.getProfileImage(withUid: uid, success: { (url) in
            do {
                let data = try Data(contentsOf: url)
                success(UIImage(data: data)!)
                
            } catch let err {
                failure(err.localizedDescription)
            }
        }, failure: { (message) in
            failure(message)
        })
    }
    
    public func getUserData(withUID uid:String, success:@escaping ()->(), failure:@escaping (String)->()) {

        userManager.getUser(withID:uid, completion: { [unowned self] (dict:[String:Any]?) in
            guard let dict = dict else {
                failure("Data Error.")
                return
            }
            self.user = User(withDict: dict)
            success()
        }) { (failMessage:String) in
            failure(failMessage)
        }
    }
    
    public func incrementUserHaircutCount(success:@escaping ()->(), failure:@escaping (String)->()) {
        guard let id = self.user?.uid else { return }
        userManager.updateUserHaircutCount(withID: id, completion: {
            success()
        }) { (errorMessage) in
            failure(errorMessage)
        }
    }
    
    public func getHaircuts(withUID uid:String, fromStart:Bool, success:@escaping ()->(), failure:@escaping (String)->()) {
        
        // get haircut data
        haircutManager.getHaircuts(withUID: uid, limit: itemsPerPage, fromStart: fromStart, success: { [unowned self] (haircuts:[Haircut]) in
            if fromStart {
                self.haircuts = haircuts
            } else {
                self.haircuts.append(contentsOf: haircuts)
            }
            success()
        }) { (message) in
            failure(message)
        }
    }
    // MARK: - Delegates
}
