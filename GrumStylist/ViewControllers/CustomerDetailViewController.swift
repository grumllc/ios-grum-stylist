//
//  CustomerDetailViewController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/11/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class CustomerDetailViewController: BaseViewController {

    // MARK: - Properties
    private var viewModel: CustomerDetailViewModel!
    private var userID:String?
    private var cellHeight:CGFloat!
    private var roundedProfileImageView:RoundedImageView = RoundedImageView()
    
    private let backgroundImageView : UIImageView = {
        let imageview = UIImageView.init(image: UIImage.init(named: "background"))
        imageview.contentMode = .scaleAspectFill
        imageview.clipsToBounds = true
        imageview.alpha = 0.75
        return imageview
    }()
    
    let addButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "nav_add"), for: .normal)
        button.backgroundColor = AppTheme().secondaryColor()
        button.layer.shadowColor = UIColor.black.withAlphaComponent(0.4).cgColor
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 2.0
        button.layer.masksToBounds = false
        button.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        button.addTarget(self, action: #selector(addButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textColor = .white
        return label
    }()
    var customSegmentedControl: CustomSegmentedControlView?

    var refreshControl = UIRefreshControl()
    private let tableView: UITableView = {
        let tableview = UITableView()
        tableview.separatorStyle = .none
        tableview.backgroundColor = .clear
        return tableview
    }()
    
    
    // MARK: - UILifeCycle
    init(userid:String?) {
        super.init(nibName: nil, bundle: nil)
        self.userID = userid
        self.hidesBottomBarWhenPushed = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.setupPlatformSpecificInputs()
        self.setupNotifications()
        self.loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .clear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.barTintColor = AppTheme().secondaryColor()
        navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - Private API
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(notification_didSaveHaircut), name: NotificationKeys.didSaveHaircut, object: nil)
    }
    
    private func setupPlatformSpecificInputs() {
        if AppUtility.isDeviceiPad() {
            self.cellHeight = 150
        } else {
            self.cellHeight = 100
        }
    }
    
    private func setup() {
        
        self.viewModel = CustomerDetailViewModel()

        self.title = ""
        // background top
        self.view.addSubview(self.backgroundImageView)
        self.backgroundImageView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(200)
        }
        
        // profile image
        self.roundedProfileImageView = RoundedImageView(width: 120, height: 120)
        self.view.addSubview(self.roundedProfileImageView)
        self.roundedProfileImageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.height.width.equalTo(120)
            make.top.equalToSuperview().offset(50)
        }
        
        // label name
        self.view.addSubview(self.nameLabel)
        self.nameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.roundedProfileImageView.snp.bottom)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(self.backgroundImageView.snp.bottom)
        }
        
        // custom segmented control
        self.customSegmentedControl = CustomSegmentedControlView(withTitles: ["All", "Favorites", "Search"], delegate: self)
        self.view.addSubview(customSegmentedControl!)
        customSegmentedControl!.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(self.backgroundImageView.snp.bottom)
            make.height.equalTo(40)
        }
        
        // separator
        let separator = UIView()
        separator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(separator)
        separator.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(customSegmentedControl!.snp.bottom)
        }
        
        // tableview
        refreshControl.addTarget(self, action: #selector(refreshControl_refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
        self.view.addSubview(self.tableView)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.snp.makeConstraints { (make) in
            make.top.equalTo(separator.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
        
        // register tableviewcells
        tableView.register(HairCutTableViewCell.self, forCellReuseIdentifier: "HairCutTableViewCell")

        // button
        self.view.addSubview(self.addButton)
        self.addButton.layer.cornerRadius = 60 / 2
        self.addButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.width.equalTo(60)
            make.height.equalTo(60)
            make.bottom.equalToSuperview().offset(-20)
        }
        
    }
    
    private func loadData() {
        self.view.bringSubviewToFront(self.activityView)

        guard let userid = self.userID else {
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: "User not found", parentVC: self, handler: {
                self.dismiss(animated: true, completion: nil)
            })
            return
        }
        
        self.activityView.animate()
        let group = DispatchGroup()
        var errorMessage:String = ""
        // get profile data
        group.enter()
        self.viewModel.getUserData(withUID: userid, success: { [unowned self] in
            DispatchQueue.main.async { [unowned self] in
                self.nameLabel.text = self.viewModel.user?.displayName
            }
            group.leave()

        }) { (message) in
            errorMessage += " " + message
            group.leave()
        }
        
        // get profile image
        group.enter()
        self.viewModel.getUserImage(withUID: userid, success: { (image) in
            
            DispatchQueue.main.async { [unowned self] in
                self.roundedProfileImageView.imageView.contentMode = .scaleAspectFill
                self.roundedProfileImageView.imageView.image = image
            }
            group.leave()

        }) { (message) in
            print(message)
            group.leave()
        }
        
        // get haircuts
        group.enter()
        self.viewModel.getHaircuts(withUID: userid, fromStart: true, success: {
            group.leave()
        }) { (message) in
            print(message)
            group.leave()
        }
        
        group.notify(queue: .main) {
            self.activityView.stopAnimate()
            if errorMessage.isEmpty {
                self.tableView.reloadData()
            } else {
                print(errorMessage)
                PresentrHelper().displaySimpleAlert(withTitle: "Error", body: (errorMessage), parentVC: self, handler:{
                    self.navigationController?.popViewController(animated: true)
                })
            }

        }
    }
    
    private func reloadTable(fromStart fromstart:Bool) {
        self.viewModel.getHaircuts(withUID: self.viewModel.user?.uid ?? "", fromStart: fromstart , success: {
            self.activityView.stopAnimate()
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }) { (message) in
            self.activityView.stopAnimate()
            self.refreshControl.endRefreshing()
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: message, parentVC: self, handler:nil)
        }
    }
    
    @objc func refreshControl_refresh(sender:AnyObject) {
        reloadTable(fromStart: true)
    }
    
    // MARK: Notifications
    @objc private func notification_didSaveHaircut(_ notification: Notification) {
        self.activityView.animate()
        // increment hair count and save
        self.viewModel.incrementUserHaircutCount(success: {
            self.reloadTable(fromStart: true)
        }) { (errorMessage) in
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: errorMessage, parentVC: self, handler:nil)
        }
        
    }
    
    // MARK: UIResponder
    @objc private func addButton_touchUpInside(sender:UIButton!) {
        let destinationVC = UINavigationController.init(rootViewController: AddHaircutViewController(userid: self.userID!))
        self.present(destinationVC, animated: true, completion: nil)
    }
    
    // MARK: - Public API
    
    
    
    // MARK: - Delegates
    
}

extension CustomerDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.haircuts.count == 0 {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No entry found"
            noDataLabel.textColor     = UIColor.black.withAlphaComponent(0.4)
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.backgroundView?.isHidden = false
        } else {
            tableView.backgroundView?.isHidden = true
        }
        return self.viewModel.haircuts.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // TODO: self.viewModel.hasMoreRecords
        if self.viewModel.hasMoreRecords() && indexPath.section == tableView.numberOfSections - 1 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "RefreshControl")
            cell.selectionStyle = .none
            cell.isUserInteractionEnabled = false
            
            let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
            activityIndicator.translatesAutoresizingMaskIntoConstraints = false
            activityIndicator.startAnimating()
            cell.addSubview(activityIndicator)
            activityIndicator.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HairCutTableViewCell", for: indexPath) as! HairCutTableViewCell
            cell.haircut = self.viewModel.haircuts[indexPath.row]
            return cell
        }

    }

    // MARK: Delegate
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if  self.viewModel.hasMoreRecords() && indexPath.section == tableView.numberOfSections - 1 && indexPath.row == tableView.numberOfRows(inSection: indexPath.section) - 1 {
            print("Load next page")
            reloadTable(fromStart: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected row \(indexPath.row)")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.cellHeight
    }
    
}

extension CustomerDetailViewController: CustomSegmentedControlViewDelegate {
    func CustomSegmentedControlViewDelegateDidSelectItem(name: String, index: Int) {
        print(name)
    }
}
