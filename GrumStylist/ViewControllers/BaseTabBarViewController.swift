//
//  TabBarController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 1/20/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class BaseTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    // MARK: Variables
    private var array:[UIViewController] = []
    
    // MARK: UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (AppUtility.isDeviceiPad()) {
            AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeLeft)
        } else {
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: Private API
    private func setup() {
        
        // home
        let homeVC = HomeViewController()
        if let uid = UserDefaults.standard.string(forKey: UserDefaultKeys.stylistID) {
            homeVC.companyUserID = uid
        }
        let firstVC = UINavigationController(rootViewController: homeVC)
        firstVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "nav_home"), selectedImage: nil)
        array.append(firstVC)


        // scan qr
        let qrVC = UINavigationController(rootViewController: QRCodeViewController())
        qrVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "nav_qr"), selectedImage: nil)
        array.append(qrVC)

        // calendar
        let calendarVC = UIViewController()
        calendarVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "nav_calendar"), selectedImage: nil)

        array.append(calendarVC)
        
        // settings
        let settingsVC = UINavigationController(rootViewController: SettingsViewController())
        settingsVC.tabBarItem = UITabBarItem(title: nil, image: UIImage(named: "nav_settings"), selectedImage: nil)
        array.append(settingsVC)
        
        
        self.viewControllers = self.array
        tabBar.itemPositioning = .fill
        tabBar.tintColor = AppTheme().secondaryColor()

        let numberOfItems = CGFloat(tabBar.items!.count)
        let tabBarItemSize = CGSize(width: tabBar.frame.width / numberOfItems, height: tabBar.frame.height)
        tabBar.selectionIndicatorImage = UIImage.imageWithColor(color: AppTheme().mainColor(), size: tabBarItemSize).resizableImage(withCapInsets: UIEdgeInsets.zero)
        
        // hides the title and moves image to center
        if let items = self.tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0);
            }
        }
    }
    
    // MARK: Public API
    
    
    // MARK: Delegates
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected \(viewController.title!)")
    }
    

}

