//
//  SettingsViewController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 1/28/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {

    // MARK: - Properties
    private let buttonLogout:UIButton = {
        let button = UIButton()
        button.setTitle("Logout", for: .normal)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.backgroundColor = .white
        button.layer.borderColor = UIColor.black.cgColor
        button.addTarget(self, action: #selector(buttonLogout_touchUpInside), for: .touchUpInside)

        return button
    }()
    
    // MARK: - UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Private API
    private func setup() {
        self.navigationItem.title = "Settings"

        // logout
        self.view.addSubview(buttonLogout)
        self.buttonLogout.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(150)
            make.height.equalTo(50)
        }
        
    }
    
    // MARK: UIResponder
    @objc private func buttonLogout_touchUpInside(sender:UIButton!) {
        Stylist.currentStylist.logout()
        self.present(LoginViewController(), animated: true, completion: nil)

    }
    
    // MARK: - Public API
    
    
    
    // MARK: - Delegates
    
}
