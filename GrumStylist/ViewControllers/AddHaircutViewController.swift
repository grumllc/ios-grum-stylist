//
//  AddHaircutViewController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/18/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class AddHaircutViewController: BaseViewController {

    // MARK: - Properties
    private static let kSectionInputIndex = 0
    private static let kSectionPhotoIndex = 1
    private var activeTextfield : UITextField?

    private var viewModel: AddHaircutViewModel!
    
    private let collectionView: UICollectionView = {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumLineSpacing = 0
        flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
        let collectionview = UICollectionView.init(frame: .zero, collectionViewLayout: flowLayout)
        collectionview.backgroundColor = .clear
        return collectionview
    }()
    
    private let barButtonIndicator: UIBarButtonItem = {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        let item = UIBarButtonItem(customView: activityIndicator)
        return item
    }()
    
    private let barButtonSave: UIBarButtonItem = {
        let item = UIBarButtonItem()
        item.tintColor = UIColor.white
        return item
    }()
    
    // MARK: - UILifeCycle
    init(userid:String) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = AddHaircutViewModel(withClientID: userid)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Private API
    private func setup() {
        self.title = "New Entry"
        
        // save button
        self.barButtonSave.title = "Save"
        self.barButtonSave.target = self
        self.barButtonSave.action = #selector(saveButton_touchUpInside)
        showButtonSave()
        
        // collectionview
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.view.addSubview(self.collectionView)
        self.collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        collectionView.register(TextFieldCollectionViewCell.self, forCellWithReuseIdentifier: "TextFieldCollectionViewCell")
        collectionView.register(PickerInputCollectionViewCell.self, forCellWithReuseIdentifier: "PickerInputCollectionViewCell")
        collectionView.register(TextViewCollectionViewCell.self, forCellWithReuseIdentifier: "TextViewCollectionViewCell")
        collectionView.register(SnapshotsCollectionViewCell.self, forCellWithReuseIdentifier: "SnapshotsCollectionViewCell")

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(view_didTap))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    private func showButtonSave() {
        self.navigationItem.rightBarButtonItem = barButtonSave
    }
    
    private func showButtonActivityIndicator() {
        self.navigationItem.rightBarButtonItem = barButtonIndicator
    }
    
    private func reloadData() {
        self.collectionView.reloadData()
    }
    
    // MARK: UIResponder
    @objc private func view_didTap(gestureRecognizer: UIGestureRecognizer) {
        self.activeTextfield?.resignFirstResponder()
    }
    @objc private func saveButton_touchUpInside(sender: UIBarButtonItem) {

        self.view.endEditing(true)
        if self.viewModel.areInputsValid() {
            self.showButtonActivityIndicator()
            self.view.isUserInteractionEnabled = false
            self.viewModel.save(withSuccess: {
                self.view.isUserInteractionEnabled = true
                self.showButtonSave()
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            }) { (message) in
                self.showButtonSave()
                self.view.isUserInteractionEnabled = true
                PresentrHelper().displaySimpleAlert(withTitle: "Error", body: message, parentVC: self, handler:{
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
        } else {
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: "Please fill out all inputs and images", parentVC: self, handler:nil)
        }
        
    }
    
    // MARK: - Public API
    
}

extension AddHaircutViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == AddHaircutViewController.kSectionInputIndex {
            return self.viewModel.inputs.count
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (indexPath.section == AddHaircutViewController.kSectionInputIndex) {
            let inputItem = self.viewModel.inputs[indexPath.row]
            switch inputItem.type! {
            case .textField:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextFieldCollectionViewCell", for: indexPath) as! TextFieldCollectionViewCell
                cell.delegate = self
                cell.textField.tag =  indexPath.row
                
                cell.input = inputItem
                return cell
                
            case .textView:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TextViewCollectionViewCell", for: indexPath) as! TextViewCollectionViewCell
                cell.delegate = self
                cell.textView.tag = indexPath.row
                cell.input = inputItem
                return cell
                
            case .datePicker:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PickerInputCollectionViewCell", for: indexPath) as! PickerInputCollectionViewCell
                cell.delegate = self
                cell.textField.tag =  indexPath.row
                cell.input = inputItem
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SnapshotsCollectionViewCell", for: indexPath) as! SnapshotsCollectionViewCell
            cell.snapshots = (self.viewModel.haircut?.snapshots)!
            cell.delegate = self
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var size = CGSize()
        let itemWidth = collectionView.bounds.width
        switch indexPath.section {
        case AddHaircutViewController.kSectionInputIndex:
            size = CGSize(width: itemWidth, height: 60)
            break
        case AddHaircutViewController.kSectionPhotoIndex:
            let height = self.view.frame.height - (60 * 3)
            size = CGSize(width: itemWidth, height: height)
            break
        default:
            break
        }
        return size
    }
}

extension AddHaircutViewController: TextFieldCollectionViewCellDelegate {
    func TextFieldCollectionViewCellDelegate(isEditingTextField textfield: UITextField) {
        print("Delegate is called")
    }
    
    func TextFieldCollectionViewCellDelegate(didPressNext textfield:UITextField) {
        if let nextField = self.view.viewWithTag(textfield.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        }
    }
    
}

extension AddHaircutViewController: PickerInputCollectionViewCellDelegate {
    func PickerInputCollectionViewCellDelegate(isEditingTextField textfield: UITextField) {
        print("Delegate is called")

    }
    
    func PickerInputCollectionViewCellDelegate(didPressDone textfield:UITextField) {
        if let nextField = self.view.viewWithTag(textfield.tag + 1) as? UITextView {
            nextField.becomeFirstResponder()
        }
    }
}

extension AddHaircutViewController: TextViewCollectionViewCellDelegate {
    func TextViewCollectionViewCellDelegate(isEditingTextView textView: UITextView) {
        print("Delegate is called")

    }
}

extension AddHaircutViewController: SnapshotsCollectionViewCellDelegate {
    
    private func getImage(fromPosition position: SnapshotPositione) -> UIImage? {
        for snapshot in (self.viewModel.haircut?.snapshots)! {
            if snapshot.position == position {
                return snapshot.image
            }
        }
        return nil
    }
    
    func SnapshotsCollectionViewCellDelegate(didSelectItem position: SnapshotPositione, status:PhotoCaptureVieweStatus) {
        
        guard let haircut = self.viewModel.haircut else { return }
        
        switch status {
        case .editing:
            // find the image for the given position
            guard let image = self.getImage(fromPosition: position) else { return }
            self.present(PhotoCaptureSnapshotViewController(image: image, delegate:self, position: position), animated: true, completion: nil)
            break
        case . adding:
            let destinationVC = PhotoCaptureViewController(position: position, snapshots:haircut.snapshots)
            destinationVC.delegate = self
            self.present(destinationVC, animated: true, completion: nil)
            break
        }
    }
}

extension AddHaircutViewController: PhotoCaptureSnapshotDelegate {
    func PhotoCaptureSnapshotDelegate(didCancel position:SnapshotPositione) {
        // retake photo
        guard let haircut = self.viewModel.haircut else { return }
        let destinationVC = PhotoCaptureViewController(position: position, snapshots:haircut.snapshots)
        destinationVC.delegate = self
        self.present(destinationVC, animated: true, completion: nil)
    }
    
    func PhotoCaptureSnapshotDelegate(didConfirm sender: PhotoCaptureSnapshotViewController, capturedImage: UIImage) {
        // do nothing
    }
}

extension AddHaircutViewController: PhotoCaptureViewControllerDelegate {
    func PhotoCaptureViewControllerDelegate(isFinishedEditing snapshots: [Snapshot]) {
        // tie all snapshots with the haircut object
        for snapshot in snapshots {
            snapshot.haircutID = self.viewModel.haircut?.haircutID
        }
        self.viewModel.haircut?.snapshots = snapshots
        self.reloadData()
    }
}
