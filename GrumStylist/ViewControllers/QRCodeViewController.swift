//
//  QRCodeViewController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 1/20/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import AVFoundation

class QRCodeViewController: BaseViewController {

    // MARK: - Properties
    private static let kQuerystringItemUID = "uid"
    private static let kQuerystringItemDisplayName = "displayname"

    private let activityIndicator:CustomActivityIndicatorView = CustomActivityIndicatorView()
    private let qrScannerView = QRScannerView()
    private let captureSession = AVCaptureSession()
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    private lazy var actionSheet: UIAlertController = {
       let actionsheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        return actionsheet
    }()
    
    private var uid:String?
    
    // MARK: - UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(self.view.frame)
        let status:AVAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        if (status == .authorized) {
            if (!self.captureSession.isRunning) {
                self.startScanning()
                self.qrScannerView.updateFrameShadow()
            }
        } else if (status == .denied) {
            PresentrHelper().displayAlertWithCallback(callback: { [unowned self] in
                self.navigateToSettings()
            }, callbackTitle: "Settings", title: "Error", body: "This app does not have access to your camera. You can enable access in Privacy Settings", parentVC: self)
            
        } else if (status == .notDetermined) {
            AVCaptureDevice.requestAccess(for: .video) { (granted) in
                if (granted) {
                    print("Camera access granted!")
                    self.startScanning()
                    self.qrScannerView.updateFrameShadow()
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (captureSession.isRunning) {
            captureSession.stopRunning()
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.updatePreviewLayerView()
    }

    
    deinit {
        print("Deinit QRVC")
    }
    
    // MARK: - Private API
    private func setup() {
        self.navigationItem.title = "Scan QR Code"
        
        // activityIndicator
        self.view.addSubview(self.activityIndicator)
        self.activityIndicator.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // scanner view
        self.view.addSubview(self.qrScannerView)
        self.qrScannerView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(280)
            make.width.equalTo(280)
        }
        
        self.actionSheet.addAction(UIAlertAction(title: "View User", style: .default , handler:{ [unowned self] (UIAlertAction)in
            guard let uid = self.uid else { return }
            self.navigateToCustomerView(uid:uid)
        }))
        
        self.actionSheet.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ [unowned self] (UIAlertAction)in
            if (!self.captureSession.isRunning) {
                self.captureSession.startRunning()
            }
        }))
    }
    
    func cameraWithPosition(_ position: AVCaptureDevice.Position) -> AVCaptureDevice?
    {
        let deviceDescoverySession = AVCaptureDevice.DiscoverySession.init(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera],
                                                                           mediaType: AVMediaType.video,
                                                                           position: AVCaptureDevice.Position.unspecified)
        for device in deviceDescoverySession.devices {
            if device.position == position {
                return device
            }
        }
        
        return nil
    }

    private func startScanning() {
        
        do {
            var captureDevicePosition = AVCaptureDevice.Position.back
            if (AppUtility.isDeviceiPad()) {
                captureDevicePosition = .front
            }
            
            guard let device = cameraWithPosition(captureDevicePosition) else { return }
            let deviceInput = try AVCaptureDeviceInput.init(device: device)
            
            if self.captureSession.canAddInput(deviceInput) {
                self.captureSession.addInput(deviceInput)
            }
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            if (self.captureSession.canAddOutput(captureMetadataOutput)) {
                self.captureSession.addOutput(captureMetadataOutput)
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            }
            
            // setup previewLayer
            DispatchQueue.main.async {
                // HACK: to fix the frame reduction on back navigation
                self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
                self.videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
                self.view.layer.addSublayer(self.videoPreviewLayer!)
                self.videoPreviewLayer?.frame = self.view.bounds
                
                // bring frame to front
                self.view.bringSubviewToFront(self.qrScannerView)
                self.view.bringSubviewToFront(self.activityIndicator)
                
                // run session
                self.captureSession.startRunning()
            }

        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
    }
    
    private func navigateToSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }
    
    private func navigateToCustomerView(uid:String) {
        print(uid)
        if (self.captureSession.isRunning) { self.captureSession.stopRunning() }
        let destinationVC = CustomerDetailViewController(userid: uid)
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
    
    private func displayActionSheet(title:String) {
        self.actionSheet.title = title
        
        // needed for iPad
        self.actionSheet.popoverPresentationController?.sourceView = self.qrScannerView
        self.present(self.actionSheet, animated: true, completion: nil)
    }
    
    private func updatePreviewLayerView() {

        DispatchQueue.main.async {
            let previewLayerConnection = self.videoPreviewLayer?.connection
            if (previewLayerConnection?.isVideoOrientationSupported ?? false) {
                let orientation = UIApplication.shared.statusBarOrientation
                switch orientation {
                case .unknown:
                    break
                case .portrait:
                    previewLayerConnection?.videoOrientation = .portrait
                    break
                case .portraitUpsideDown:
                    previewLayerConnection?.videoOrientation = .portraitUpsideDown
                    break
                case .landscapeLeft:
                    previewLayerConnection?.videoOrientation = .landscapeLeft
                    break
                case .landscapeRight:
                    previewLayerConnection?.videoOrientation = .landscapeRight
                    break
                }
            }
        }
    }
    
    private func getUIDFromURL(value:String) -> String? {
        let queryItems = URLComponents(string: value)?.queryItems
        guard let uid = queryItems?.filter({$0.name == QRCodeViewController.kQuerystringItemUID}).first else { return nil }
        return uid.value!
    }
    
    private func getDisplayNameFromURL(value:String) -> String? {
        let queryItems = URLComponents(string: value)?.queryItems
        guard let displayName = queryItems?.filter({$0.name == QRCodeViewController.kQuerystringItemDisplayName}).first else { return nil }
        return displayName.value!
    }
    
    // MARK: - Public API
    
    
    
    // MARK: - Delegates
    
}


extension QRCodeViewController: AVCaptureMetadataOutputObjectsDelegate {
 
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if (metadataObjects.count == 0) {
            return
        }
        guard let metadataObject = metadataObjects.first else { return }
        if metadataObject.type == .qr {
            guard let qrObject = self.videoPreviewLayer?.transformedMetadataObject(for: metadataObject) else { return }
            if (self.qrScannerView.isSurroundingItem(itemBounds: qrObject.bounds)) {
                guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                guard let stringValue = readableObject.stringValue else { return }
                
                if (self.presentedViewController == nil && stringValue.localizedLowercase.contains("grum-app")) {
                    print(stringValue)
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    guard let displayName = self.getDisplayNameFromURL(value: stringValue) else { return }
                    guard let uid = self.getUIDFromURL(value: stringValue) else { return }

                    // store the uid
                    self.uid = uid
                    self.displayActionSheet(title: displayName)
                }
                
            }
            
        }
    }
}
