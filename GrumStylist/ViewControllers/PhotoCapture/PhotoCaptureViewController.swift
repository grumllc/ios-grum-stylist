//
//  PhotoCaptureViewController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/20/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import AVFoundation

protocol PhotoCaptureViewControllerDelegate: class {
    func PhotoCaptureViewControllerDelegate(isFinishedEditing snapshots:[Snapshot])
}
class PhotoCaptureViewController: BaseViewController {

    // MARK: - Properties
    public weak var delegate: PhotoCaptureViewControllerDelegate?
    private var captureSession = AVCaptureSession()
    private var backCamera: AVCaptureDevice?
    private var frontCamera: AVCaptureDevice?
    private var currentCamera: AVCaptureDevice?
    private var photoOutput: AVCapturePhotoOutput?
    private var cameraPreviewLayer: AVCaptureVideoPreviewLayer?
    private var viewModel: PhotoCaptureViewModel?
    private var captureVideoOrientation: AVCaptureVideoOrientation?
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textColor = .white
        return label
    }()
    private let closeButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "nav_close"), for: .normal)
        button.addTarget(self, action: #selector(closeButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    private let swapButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "camera_swap"), for: .normal)
        button.addTarget(self, action: #selector(swapButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    private let captureButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 5
        button.addTarget(self, action: #selector(captureButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    // MARK: - UILifeCycle
    init(position:SnapshotPositione, snapshots:[Snapshot]?) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = PhotoCaptureViewModel()
        self.viewModel?.snapshots = snapshots ?? [Snapshot]()
        self.viewModel?.snapShotPosition = position

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupNotifications()
        self.getDeviceSpecificSettings()
        self.checkPermission()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !self.captureSession.isRunning {
            self.captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    

    deinit {
        print("deinit PhotoCaptureVC")
        self.captureSession.stopRunning()

    }
    
    // MARK: - Private API
    private func getDeviceSpecificSettings() {
        if AppUtility.isDeviceiPad() {
            captureVideoOrientation = AVCaptureVideoOrientation.landscapeLeft
        } else {
            captureVideoOrientation = AVCaptureVideoOrientation.portrait
        }
    }
    
    private func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(notification_didFinishTakingSnapshots), name: NotificationKeys.didFinishTakingSnapshots, object: nil)
    }
    
    private func setup() {
        self.view.backgroundColor = .black
        self.titleLabel.text = self.viewModel?.title
        
        // title label
        self.view.addSubview(self.titleLabel)
        self.titleLabel.snp.makeConstraints { (make) in
            make.topMargin.equalToSuperview().offset(10)
            make.centerX.equalToSuperview()
        }
        
        // double tap gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(view_doubleTapped))
        tap.numberOfTapsRequired = 2
        view.addGestureRecognizer(tap)
        
        // close button
        self.view.addSubview(self.closeButton)
        self.closeButton.snp.makeConstraints { (make) in
            make.topMargin.left.equalToSuperview().offset(10)
            make.width.height.equalTo(40)
        }
        
        // swap button
        self.view.addSubview(self.swapButton)
        self.swapButton.snp.makeConstraints { (make) in
            make.topMargin.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.width.height.equalTo(40)
        }
        
        // capture button
        self.view.addSubview(self.captureButton)
        self.captureButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-20)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(80)
        }
        self.captureButton.layer.cornerRadius = 80 / 2
        
        setupCaptureSession()
        setupDevice()
        setupInputOutput()
        setupPreviewLayer()
        startRunningCaptureSession()
        
    }
    
    private func checkPermission() {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            self.setup()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { [unowned self] granted in
                self.setup()
            }
        default:
            PresentrHelper().displayAlertWithCallback(callback: { [unowned self] in
                self.navigateToSettings()
                }, callbackTitle: "Settings", title: "Error", body: "This app does not have access to your camera. You can enable access in Privacy Settings", parentVC: self)
        }
    }
    
    private func navigateToSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: nil)
        }
    }
    
    private func setupCaptureSession() {
        self.captureSession.sessionPreset = AVCaptureSession.Preset.photo
    }
    
    private func setupDevice() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: .video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCamera = device
            } else if device.position == AVCaptureDevice.Position.front {
                frontCamera = device
            }
        }
        if (AppUtility.isDeviceiPad()) {
            currentCamera = frontCamera
        } else {
            currentCamera = backCamera
        }
    }
    
    private func setupInputOutput() {
        do {
            // clear out all input / output first
            for oldInput:AVCaptureInput in captureSession.inputs {
                captureSession.removeInput(oldInput)
            }

            for oldOutput:AVCaptureOutput in captureSession.outputs {
                captureSession.removeOutput(oldOutput)
            }
            let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera!)
            captureSession.addInput(captureDeviceInput)
            
            photoOutput = AVCapturePhotoOutput()
            photoOutput!.isHighResolutionCaptureEnabled = true
            photoOutput!.isLivePhotoCaptureEnabled = photoOutput!.isLivePhotoCaptureSupported
            if let photoOutputConnection = self.photoOutput!.connection(with: .video) {
                photoOutputConnection.videoOrientation = captureVideoOrientation!
            }

            photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
            self.captureSession.sessionPreset = .photo
            self.captureSession.addOutput(photoOutput!)

        } catch {
            print(error)
        }

    }
    
    private func setupPreviewLayer() {
         DispatchQueue.main.async {
            self.cameraPreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
            print((self.cameraPreviewLayer?.connection?.videoOrientation == .landscapeLeft) ? "YES" : "NO")
            self.cameraPreviewLayer?.connection?.videoOrientation = self.captureVideoOrientation!
            print((self.cameraPreviewLayer?.connection?.videoOrientation == .landscapeLeft) ? "YES" : "NO")

            self.cameraPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            self.cameraPreviewLayer?.frame = self.view.bounds
            self.view.layer.insertSublayer(self.cameraPreviewLayer!, at: 0)
        }
    }
    
    private func startRunningCaptureSession() {
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }
    
    private func swapCamera() {
        currentCamera = (currentCamera == backCamera) ? frontCamera : backCamera
        captureSession.beginConfiguration()
        setupInputOutput()
        captureSession.commitConfiguration()
        startRunningCaptureSession()
    }
    
    // MARK: Notifications
    @objc private func notification_didFinishTakingSnapshots(_ notification: Notification) {
        guard let snapshots = self.viewModel?.snapshots else { return }
        self.dismiss(animated: true) {
            self.delegate?.PhotoCaptureViewControllerDelegate(isFinishedEditing: snapshots)
        }
    }
    
    // MARK: UIResponder
    @objc private func view_doubleTapped(gestureRecognizer: UIGestureRecognizer) {
        swapCamera()
    }
    
    @objc private func closeButton_touchUpInside(sender:UIButton!) {
        self.dismiss(animated: true, completion: {
            guard let snapshots = self.viewModel?.snapshots else { return }
            self.delegate?.PhotoCaptureViewControllerDelegate(isFinishedEditing: snapshots)
        })
    }
    
    @objc private func captureButton_touchUpInside(sender:UIButton!) {
        let settings = AVCapturePhotoSettings()
        settings.isAutoStillImageStabilizationEnabled = true
        
        let photoSettings: AVCapturePhotoSettings
        if self.photoOutput!.availablePhotoCodecTypes.contains(.hevc) {
            photoSettings = AVCapturePhotoSettings(format:
                [AVVideoCodecKey: AVVideoCodecType.hevc])
        } else {
            photoSettings = AVCapturePhotoSettings()
        }
        photoOutput?.capturePhoto(with: photoSettings, delegate: self)
    }
    
    @objc private func swapButton_touchUpInside(sender:UIButton!) {
        swapCamera()
    }
    
    // MARK: - Public API

}

extension PhotoCaptureViewController: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        if let imageData = photo.fileDataRepresentation() {
            var capturedImage = UIImage(data: imageData)
            if currentCamera == frontCamera {
                capturedImage = UIImage(cgImage: capturedImage!.cgImage!, scale: capturedImage!.scale, orientation: .leftMirrored)
            }
            if AppUtility.isDeviceiPad() {
                capturedImage = capturedImage!.rotate(radians: .pi/2) // orientation handling on ipad
            }
            
            guard let position = self.viewModel?.snapShotPosition else { return }
            self.present(PhotoCaptureSnapshotViewController(image: capturedImage!, delegate:self, position: position), animated: false, completion: nil)
        }
    }
}

extension PhotoCaptureViewController: PhotoCaptureSnapshotDelegate {
    func PhotoCaptureSnapshotDelegate(didCancel position:SnapshotPositione) {
        // do nothing
    }
    
    func PhotoCaptureSnapshotDelegate(didConfirm sender: PhotoCaptureSnapshotViewController, capturedImage:UIImage) {
        
        self.viewModel?.updateSnapshot(image: capturedImage)
        if (self.viewModel?.photoCaptureViewStatus == .editing) {
            guard let snapshots = self.viewModel?.snapshots else { return }
            self.delegate?.PhotoCaptureViewControllerDelegate(isFinishedEditing: snapshots)
            self.dismiss(animated: false, completion: nil)
        }
        // TODO: can add observable for title
        DispatchQueue.main.async {
            self.titleLabel.text = self.viewModel?.title
        }
    }
}
