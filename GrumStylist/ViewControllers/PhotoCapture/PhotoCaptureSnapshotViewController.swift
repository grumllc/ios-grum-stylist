//
//  PhotoCaptureSnapshotViewController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/20/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

protocol PhotoCaptureSnapshotDelegate: class {
    func PhotoCaptureSnapshotDelegate(didCancel position:SnapshotPositione)
    func PhotoCaptureSnapshotDelegate(didConfirm sender: PhotoCaptureSnapshotViewController, capturedImage:UIImage)

}
class PhotoCaptureSnapshotViewController: BaseViewController {

    // MARK: - Properties
    public weak var delegate: PhotoCaptureSnapshotDelegate?
    private let viewModel: PhotoCaptureSnapshotViewModel = PhotoCaptureSnapshotViewModel()
    public var capturedImage:UIImage?
    private var imageView: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        return imageview
    }()
    
    private let cancelButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "photo_cancel"), for: .normal)
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.addTarget(self, action: #selector(cancelButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    private let confirmButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage.init(named: "photo_confirm"), for: .normal)
        button.contentVerticalAlignment = .fill
        button.contentHorizontalAlignment = .fill
        button.addTarget(self, action: #selector(confirmButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    // MARK: - UILifeCycle
    init(image:UIImage, delegate:PhotoCaptureSnapshotDelegate, position: SnapshotPositione) {
        super.init(nibName: nil, bundle: nil)
        self.capturedImage = image
        self.delegate = delegate
        self.viewModel.snapShotPosition = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.displayPhoto()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Private API
    private func setup() {
        
        self.view.addSubview(self.imageView)
        self.imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // cancel button
        self.view.addSubview(self.cancelButton)
        self.cancelButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-20)
            make.left.equalToSuperview().offset(10)
            make.width.height.equalTo(64)
        }
        
        // confirm button
        self.view.addSubview(self.confirmButton)
        self.confirmButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-20)
            make.right.equalToSuperview().offset(-10)
            make.width.height.equalTo(64)
        }
    }
    
    private func displayPhoto() {
        self.imageView.image = self.capturedImage!
    }
    
    // MARK: UIResponder
    @objc private func cancelButton_touchUpInside(sender:UIButton!) {
        self.dismiss(animated: false, completion: {
            self.delegate?.PhotoCaptureSnapshotDelegate(didCancel: self.viewModel.snapShotPosition)
        })
    }
    
    @objc private func confirmButton_touchUpInside(sender:UIButton!) {
        self.dismiss(animated: true) {
            self.delegate?.PhotoCaptureSnapshotDelegate(didConfirm: self, capturedImage:self.capturedImage!)
        }
    }
    
    // MARK: - Public API
    
    
    
    
}
