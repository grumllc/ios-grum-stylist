//
//  TabBarController.swift
//  GrumStylist
//
//  Created by Daniel Yo on 1/20/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class BaseTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    // MARK: Variables
    private var array:[UIViewController] = []
    
    // MARK: UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: Private API
    private func setup() {
        
        // add the viewControllers
        let firstVC = HomeViewController()
        firstVC.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "nav_home"), selectedImage: nil)
        array.append(firstVC)
        
        self.tabBar.tintColor = .black
        self.viewControllers = self.array
    }
    
    // MARK: Public API
    
    
    // MARK: Delegates
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("Selected \(viewController.title!)")
    }
    

}
