//
//  LoginViewController.swift
//  Grum
//
//  Created by Daniel Yo on 12/19/18.
//  Copyright © 2018 Daniel Yo. All rights reserved.
//

import UIKit
import Firebase
import SWRevealViewController
import Presentr

class LoginViewController: UIViewController, UITextFieldDelegate {

    private var companyUserID : String?
    private var password : String?
    private var activeTextfield : UITextField?
    private let activityView = CustomActivityIndicatorView()
    private let backgroundImageView : UIImageView = {
        let imageview = UIImageView.init(image: UIImage.init(named: "background"))
        return imageview
    }()
    
    private let scrollview : UIScrollView = {
       let scrollview = UIScrollView()
        
        return scrollview
    }()
    private let companyUserIDTextField : UITextField = {
        let textfield = UITextField()
        textfield.textAlignment = .center
        textfield.layer.borderColor = UIColor.black.cgColor
        textfield.layer.borderWidth = 1
        textfield.layer.cornerRadius = 5
        textfield.placeholder = "Enter company userID"
        textfield.backgroundColor = UIColor.white

        return textfield
    }()
    
    private let passwordTextField : UITextField = {
        let textfield = UITextField()
        textfield.textAlignment = .center
        textfield.layer.borderColor = UIColor.black.cgColor
        textfield.layer.borderWidth = 1
        textfield.layer.cornerRadius = 5
        textfield.isSecureTextEntry = true
        textfield.placeholder = "Enter password"
        textfield.backgroundColor = UIColor.white
        return textfield
    }()
    
    private let submitButton : UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        button.setTitle("Sign In", for: .normal)
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(submitButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    
    private let logoImageView : UIImageView = {
        let imageview = UIImageView.init(image: UIImage.init(named: "app_logo"))
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()

    // MARK: - UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.registerNotifications()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(view_didTap))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (AppUtility.isDeviceiPad()) {
            AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeLeft)
        } else {
            AppUtility.lockOrientation(.all, andRotateTo: .portrait)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Private API
    private func setup() {
        // background img
        self.view.addSubview(backgroundImageView)
        backgroundImageView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        self.view.addSubview(scrollview)
        self.scrollview.addSubview(logoImageView)
        self.scrollview.addSubview(companyUserIDTextField)
        self.scrollview.addSubview(passwordTextField)
        self.scrollview.addSubview(submitButton)
        
        self.companyUserIDTextField.delegate = self
        self.passwordTextField.delegate = self

        self.scrollview.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        

        // username
        self.companyUserIDTextField.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(300)
            make.height.equalTo(50)
        }
        
        // logo
        self.logoImageView.snp.makeConstraints { (make) in
            make.bottom.equalTo(self.companyUserIDTextField.snp.top).offset(-15)
            make.centerX.equalToSuperview().offset(15)
            make.width.equalTo(240)
            make.height.equalTo(128)
        }
        
        // password
        self.passwordTextField.snp.makeConstraints { (make) in
            make.top.equalTo(self.companyUserIDTextField.snp.bottom).offset(10)
            make.left.equalTo(self.companyUserIDTextField)
            make.width.equalTo(300)
            make.height.equalTo(50)
        }
        
        self.submitButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.passwordTextField.snp.bottom).offset(10)
            make.centerX.equalToSuperview()
            make.width.equalTo(240)
            make.height.equalTo(50)
        }
        
        // activity indicator
        self.view.addSubview(self.activityView)
        self.activityView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

    // TODO: can modularize this later
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        guard let _ = self.activeTextfield else {
            return
        }
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let bottomY = self.activeTextfield!.frame.origin.y + self.activeTextfield!.frame.height
        
        if (bottomY > keyboardFrame.origin.y) {
            UIView.animate(withDuration: 0.25) {
                self.scrollview.setContentOffset(CGPoint(x: 0, y: -(keyboardFrame.origin.y - bottomY - 5)), animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.scrollview.frame.origin.y = 0
    }
    
    private func login(withCompanyUserID companyID:String?, password:String?) {

        self.activityView.animate()
        weak var weakself = self
        guard let _ = companyID, let _ = password else {
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: "Invalid username and password", parentVC: self, handler:nil)
            self.activityView.stopAnimate()
            return
        }
        
        FSStylistManager().getStylist(withID: companyID!, completion: { (company) in
            guard let strongSelf = weakself else { return }
            strongSelf.activityView.stopAnimate()
            Stylist.currentStylist.contactEmail = company.contactEmail
            Stylist.currentStylist.displayName = company.displayName
            Stylist.currentStylist.stylistID = company.stylistID
            UserDefaults.standard.set(company.stylistID, forKey: UserDefaultKeys.stylistID)

            
            UIApplication.shared.windows.first?.rootViewController = BaseTabBarViewController()//UINavigationController(rootViewController: BaseTabBarViewController())


        }) { (errorMessage) in
            guard let strongSelf = weakself else { return }
            strongSelf.activityView.stopAnimate()
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: "Invalid username and password", parentVC: self, handler:nil)
            
        }

    }
    
    // MARK: UIResponder
    @objc private func submitButton_touchUpInside(sender:UIButton!) {
        self.view.endEditing(true)
         self.login(withCompanyUserID: companyUserID, password: password)
    }
    
    @objc private func view_didTap(gestureRecognizer: UIGestureRecognizer) {
        self.activeTextfield?.resignFirstResponder()
    }
    // MARK: - Public API
    
    
    
    // MARK: - Delegates
    // MARK: UITextFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextfield = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == companyUserIDTextField) {
            companyUserID = textField.text?.lowercased()
        } else if (textField == passwordTextField) {
            password = textField.text
        }
        print(textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if (textField == companyUserIDTextField) {
            passwordTextField.becomeFirstResponder()
        } else if (textField == passwordTextField) {
            self.login(withCompanyUserID: companyUserID, password: password)
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if (textField == companyUserIDTextField) {
            companyUserID = nil
        } else if (textField == passwordTextField) {
            password = nil
        }
        return true
    }
}

