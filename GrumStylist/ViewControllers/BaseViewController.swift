//
//  BaseViewController.swift
//  Grum
//
//  Created by Daniel Yo on 12/19/18.
//  Copyright © 2018 Daniel Yo. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    // MARK: Properties
    public let activityView = CustomActivityIndicatorView()

    // MARK: UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (AppUtility.isDeviceiPad()) {
            AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeLeft)
        } else {
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        }
        

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (self.presentingViewController != nil) {
            let closeButton = UIBarButtonItem(image: UIImage.init(named: "nav_close"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(buttonClose_touchUpInside(sender:)))
            closeButton.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = closeButton
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: Private API
    private func setup() {

        self.view.backgroundColor = .white
        self.navigationItem.backBarButtonItem = UIBarButtonItem()
        
        // activity indicator
        self.view.addSubview(self.activityView)
        self.activityView.isUserInteractionEnabled = false
        self.activityView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    @objc private func buttonClose_touchUpInside(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: Public API
    
    
    
    // MARK: Delegates
    
}
