//
//  HomeViewController.swift
//  Grum
//
//  Created by Daniel Yo on 1/9/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    // MARK: Properties
    private let imageViewLogo: UIImageView = {
        let imageview = UIImageView(image: UIImage(named: "stylist_icon"))
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    private let backgroundImageView : UIImageView = {
        let imageview = UIImageView.init(image: UIImage.init(named: "background"))
        imageview.alpha = 0.4
        imageview.contentMode = .scaleAspectFill
        return imageview
    }()
    
    private let paymentButton : UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(UIImage(named: "nav_card"), for: UIControl.State.normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.contentHorizontalAlignment = .fill
        button.contentVerticalAlignment = .fill
        button.addTarget(self, action: #selector(paymentButton_touchUpInside), for: .touchUpInside)
        return button
    }()
    
    private var _companyUserID:String?
    public var companyUserID:String? {
        get {
            return _companyUserID
        }
        set {
           Stylist.currentStylist.stylistID = newValue
            _companyUserID = newValue
            guard let _ = newValue else { return }
            self.loadCompany(withID: newValue!)

        }
    }
    
    // MARK: UILifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppUtility.isDeviceiPad() {
            self.setupiPad()
        } else {
            self.setupiPhone()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: Private API
    private func setupiPad() {
        
    }
    
    private func setupiPhone() {
        self.navigationItem.title = "Home"
        
        // bg image
        self.view.addSubview(self.backgroundImageView)
        self.backgroundImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        // logo
        self.view.addSubview(self.imageViewLogo)
        self.imageViewLogo.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview().offset(10)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(200)
        }
        
        // payment
        let offset = self.view.frame.width / 4
        self.view.addSubview(paymentButton)
        self.paymentButton.snp.makeConstraints { (make) in
            make.top.equalTo(self.imageViewLogo.snp.bottom)
            make.width.height.equalTo(72)
            make.centerX.equalToSuperview().offset(offset)
        }

    }
    
    private func loadCompany(withID id:String) {
        weak var weakself = self
        FSStylistManager().getStylist(withID: id, completion: { (company) in
            guard let strongSelf = weakself else { return }
            strongSelf.activityView.stopAnimate()
            Stylist.currentStylist.contactEmail = company.contactEmail
            Stylist.currentStylist.displayName = company.displayName
            Stylist.currentStylist.stylistID = company.stylistID
            UserDefaults.standard.set(company.stylistID, forKey: UserDefaultKeys.stylistID)
            DispatchQueue.main.async {
                strongSelf.navigationItem.title = "\(company.displayName!)"
            }

        }) { (errorMessage) in
            guard let strongSelf = weakself else { return }
            strongSelf.activityView.stopAnimate()
            PresentrHelper().displaySimpleAlert(withTitle: "Error", body: "Invalid username and password", parentVC: self, handler: { [unowned self] in
                Stylist.currentStylist.logout()
                self.present(LoginViewController(), animated: true, completion: nil)
            })
        }
    }

    // MARK: UIResponder
    @objc private func paymentButton_touchUpInside(sender:UIButton!) {
        PresentrHelper().displaySimpleAlert(withTitle: "Secure Pay", body: "Not yet available", parentVC: self, handler:nil)
    }
    // MARK: Public API
    
    
    
    // MARK: Delegates
    
}
