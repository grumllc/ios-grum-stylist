//
//  FSUserManager.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/11/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import FirebaseFirestore

class FSUserManager: NSObject {
    // MARK: - Properties
    let db = Firestore.firestore()
    let collectionName = "users"
    // MARK: - Initialization
    private func customInit() {
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }
    override init() {
        super.init()
        self.customInit()
    }
    
    // MARK: - Private API
    
    // MARK: - Public API
    public func getUser(withID id:String, completion:@escaping ([String:Any]) -> (), failure:@escaping (String) -> ()) {
        
        let userRef = db.collection(collectionName)
        
        userRef.whereField("uid", isEqualTo: id).getDocuments() { (querySnapshot, err) in
            if let err = err {
                failure(err.localizedDescription)
            } else {
                if (querySnapshot!.documents.count > 0) {
                    print("\(querySnapshot!.documents) => \(querySnapshot!.documents.first!.data())")
                    completion(querySnapshot!.documents.first!.data())
                } else {
                    failure("User not found.")
                }
                
            }
        }
    }
    
    public func updateUserHaircutCount(withID id:String, completion:@escaping () -> (), failure:@escaping (String) -> ()) {
        
        self.getUser(withID: id, completion: { (dict) in
            let user = User(withDict: dict)
            user.totalHaircuts += 1
            let userRef = self.db.collection(self.collectionName).document(id)
            userRef.updateData([
                "totalHaircuts" : user.totalHaircuts,
                "lastUpdated": FieldValue.serverTimestamp(),
            ]) { (error) in
                if let err = error {
                    failure(err.localizedDescription)
                } else {
                    completion()
                }
            }
        }) { (errorMessage) in
            failure(errorMessage)
        }

    }
}
