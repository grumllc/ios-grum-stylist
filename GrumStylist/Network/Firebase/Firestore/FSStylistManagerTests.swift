//
//  FSCompanyManagerTests.swift
//  GrumStylistTests
//
//  Created by Daniel Yo on 1/19/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import XCTest
@testable import GrumStylist

class FSStylistManagerTests: XCTestCase {

    private var stylist:Stylist?
    
    override func setUp() {
        stylist = Stylist()
    }

    override func tearDown() {
        stylist = nil
    }

    func testAddStylist() {
        let expectation = self.expectation(description: "testAddStylist")

        stylist!.displayName = "Great Clips"
        stylist!.contactEmail = "greatclips@barber.com"
        stylist!.stylistID = "greatclips1"
        stylist!.password = "Password1"
        FSStylistManager().addStylist(stylist: stylist!, completion: {
            XCTAssert(true)
            expectation.fulfill()
        }) { (errorMessage) in
            XCTAssertEqual(errorMessage.lowercased(), "company already exists")
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 10, handler: nil)

    }

}
