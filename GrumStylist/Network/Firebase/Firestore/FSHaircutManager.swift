//
//  FSHaircutManager.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/27/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import FirebaseFirestore

class FSHaircutManager: NSObject {
    // MARK: - Properties
    let db = Firestore.firestore()
    let collectionName = "haircuts"
    private var lastDocumentRetreived: DocumentSnapshot?
    
    // MARK: - Initialization
    private func customInit() {
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }
    override init() {
        super.init()
        self.customInit()
    }
    
    // MARK: - Private API
    
    
    // MARK: - Public API
    public func addHaircut(requestModel: AddHaircutRequestModel, success:@escaping () -> (), failure:@escaping (String) -> ()) {
        
        // firestore/haircuts/haircutID/{obj}
        let haircutRef = db.collection(collectionName)
        haircutRef.document(requestModel.haircut!.haircutID!).setData(requestModel.dictionary) { (error) in
            if let error = error {
                print("Error adding document: \(error)")
                failure(error.localizedDescription)
            } else {
                success()
            }
        }
        
    }
    
    public func getHaircuts(withUID userID:String, limit:Int, fromStart:Bool, success:@escaping ([Haircut]) -> (), failure:@escaping (String) -> ()) {
        
        if fromStart {
            lastDocumentRetreived = nil
        }
        let haircutRef = db.collection(collectionName)
        var query:Query!
        if let lastDocument = self.lastDocumentRetreived {
            query = haircutRef.whereField("clientID", isEqualTo: userID).limit(to: limit).start(afterDocument: lastDocument)
        } else {
            query = haircutRef.whereField("clientID", isEqualTo: userID).limit(to: limit)
        }
        
        query.getDocuments() { (querySnapshot, err) in
            if let err = err {
                failure(err.localizedDescription)
            } else {
                if (querySnapshot!.documents.count > 0) {
                    var haircuts = [Haircut]()
                    for document in querySnapshot!.documents {
                        let dict = document.data() as [String: Any]
                        haircuts.append(Haircut(withDict: dict))
                    }
                    self.lastDocumentRetreived = querySnapshot!.documents.last
                    success(haircuts)
                } else {
                    failure("no document found.")
                }
                
            }
        }
    }
    // MARK: - Delegates
}
