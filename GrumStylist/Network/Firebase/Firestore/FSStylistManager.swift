//
//  FirestoreUserManager.swift
//  Grum
//
//  Created by Daniel Yo on 1/16/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import FirebaseFirestore

class FSStylistManager: NSObject {
    // MARK: - Properties
    let db = Firestore.firestore()
    let collectionName = "stylists"
    // MARK: - Initialization
    private func customInit() {
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
    }
    override init() {
        super.init()
        self.customInit()
    }
    
    // MARK: - Private API
    
    // MARK: - Public API
    // TODO: for testing only, not to be used in production
    public func addStylist(stylist:Stylist, completion:@escaping () -> (), failure:@escaping (String) -> ()) {
        guard let _ = stylist.displayName,let  _ = stylist.contactEmail, let _ = stylist.stylistID, let _ = stylist.password else { failure("Missing properties on User"); return }
        
        weak var weakself = self
        self.getStylist(withID: stylist.stylistID!, completion: { (stylistDict) in
            failure("Stylist already exists")
        }) { (errorMessage) in
            
            // if it doesnt exist, try to add it
            var ref: DocumentReference? = nil
            
            guard let sself = weakself else { return }
            ref = sself.db.collection(self.collectionName).addDocument(data: stylist.dictionary, completion: { (error) in
                if let error = error {
                    print("Error adding document: \(error)")
                    failure(error.localizedDescription)
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                    completion()
                }
            })
        }
    }
    
    public func getStylist(withID id:String, completion:@escaping (Stylist) -> (), failure:@escaping (String) -> ()) {
       
        let userRef = db.collection(collectionName)

        userRef.whereField("stylistID", isEqualTo: id).getDocuments() { (querySnapshot, err) in
            if let err = err {
                failure(err.localizedDescription)
            } else {
                if (querySnapshot!.documents.count > 0) {
                    let dict = querySnapshot!.documents.first!.data()
                    completion(Stylist.init(withDict: dict))
                } else {
                    failure("no document found")
                }

            }
        }
    }
    
    // MARK: - Delegates
}
