//
//  FirebaseStorageManager.swift
//  Grum
//
//  Created by Daniel Yo on 2/12/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import UIKit
import FirebaseStorage

class FirebaseStorageManager {
    
    // MARK: - Properties
    let storage = Storage.storage()
    var storageRef:StorageReference?
    // MARK: - Initialization
    private func customInit() {
        
    }
    init() {
        self.customInit()
        storageRef = storage.reference()
    }
    
    // MARK: - Private API
    
    // MARK: - Public API
    // MARK: User Profile
    public func getProfileImage(withUid uid:String, success:@escaping (_ url:URL)->(), failure: @escaping (_ errorMessage:String) -> ()) {
        
        let filename = "profile.jpg"
        // create reference to user path
        let userRef = storageRef!.child("users").child(uid).child(filename)
        
        // TODO: may need to put a timeout of 10 seconds for the future
        // Firebase automatically retries a certain number of times
        userRef.downloadURL { (url, err) in
            if let error = err as NSError? {
                guard let error = StorageErrorCode(rawValue: error.code) else { return }
                switch (error) {
                case .objectNotFound:
                    failure("File not found.")
                    break
                case .unauthorized:
                    failure("You do not have access to download image.")
                    break
                case .cancelled:
                    failure("An unknown error has occured, please try again later.")
                    // User cancelled the download
                    break
                    
                case .unknown:
                    failure("An unknown error has occured, please try again later.")
                    break
                default:
                    failure("DEFAULT ERROR HAS OCCURED.") // TODO: to be updated later
                    // Another error occurred. This is a good place to retry the download.
                    break
                }
            } else {
                success(url!)
            }
        }
    }
    
    // MARK: Snapshots
    public func saveSnapshotImage(withRequestModel model:SaveSnapshotImageRequestModel, success:@escaping (_ url:String)->(), failure: @escaping (_ errorMessage:String) -> ()) {
        
        let filename = "\(model.position).jpg"
        let folderName = "snapshots"
        // create reference to user path
        let snapshotRef = storageRef!.child(folderName).child(model.haircutID).child(filename)
        
        // Upload the file to the path "users/uid/profile.jpg"
        let data = model.image.jpegData(compressionQuality: 0.75)
        let uploadTask = snapshotRef.putData(data!, metadata: nil)
        
        uploadTask.observe(.success) { snapshot in
            guard let _ = snapshot.metadata else {
                failure("Snapshot data not found.")
                return
            }
            snapshotRef.downloadURL(completion: { (url, err) in
                if let error = err as NSError? {
                    guard let error = StorageErrorCode(rawValue: error.code) else { return }
                    switch (error) {
                    case .objectNotFound:
                        failure("File not found.")
                        break
                    case .unauthorized:
                        failure("You do not have access to download image.")
                        break
                    case .cancelled:
                        failure("An unknown error has occured, please try again later.")
                        // User cancelled the download
                        break
                        
                    case .unknown:
                        failure("An unknown error has occured, please try again later.")
                        break
                    default:
                        failure("DEFAULT ERROR HAS OCCURED.") // TODO: to be updated later
                        break
                    }
                } else {
                    guard let url = url else { failure("Invalid URL"); return }
                    success(url.absoluteString)
                }
            })
            
        }
        
        uploadTask.observe(.failure) { snapshot in
            if let error = snapshot.error as NSError? {
                switch (StorageErrorCode(rawValue: error.code)!) {
                case .objectNotFound:
                    failure("File not found.")
                    break
                case .unauthorized:
                    failure("You do not have access to save image.")
                    break
                case .cancelled:
                    // User canceled the upload
                    break
                    
                case .unknown:
                    failure("An unknown error has occured, please try again later.")
                    break
                default:
                    failure("DEFAULT ERROR HAS OCCURED.") // TODO: to be updated later
                    // A separate error occurred. This is a good place to retry the upload.
                    break
                }
            }
        }
    }
    
}
