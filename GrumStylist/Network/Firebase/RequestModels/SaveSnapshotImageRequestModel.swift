//
//  SaveSnapshotsRequestModel.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/27/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import Foundation
import UIKit
class SaveSnapshotImageRequestModel {
    var image: UIImage
    var position: String
    var haircutID: String
    
    init(withImage image: UIImage, position:String, haircutID:String) {
        self.image = image
        self.position = position
        self.haircutID = haircutID
    }
}
