//
//  AddHaircutRequestModel.swift
//  GrumStylist
//
//  Created by Daniel Yo on 2/27/19.
//  Copyright © 2019 Daniel Yo. All rights reserved.
//

import Foundation
import UIKit
class AddHaircutRequestModel {
    var haircut: Haircut?
    
    var dictionary: [String: Any] {
        return [
            "stylist": haircut!.stylist!,
            "stylistID": haircut!.stylistID!,
            "clientID": haircut!.clientID!,
            "haircutID": haircut!.haircutID!,
            "dateTaken": haircut!.dateTaken!,
            "shortDescription": haircut!.shortDescription!,
            "snapshots" : Dictionary(uniqueKeysWithValues: haircut!.snapshots!.map { ($0.position?.toString, $0.dictionary ) }),
        ]
    }
    
    init(withHaircut haircut: Haircut) {
        self.haircut = haircut
    }
}
